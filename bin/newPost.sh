#!/bin/bash

declare -a questions=(
  "What is the post title?"
)
declare -a answers

for question in "${questions[@]}"; do
    clear
    echo "${question}"
    read tmp
    answers+=("$tmp")
done

TITLE=$(node ./bin/newPost.js -t "${answers[0]}")

clear
git checkout -b wrote/${TITLE}
git add .
git commit -m"Files created for ${TITLE}"
git status
