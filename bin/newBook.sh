#!/bin/bash

declare -a questions=(
  "What is the book title?"
  "What is the book author?"
  "What is the purchase link?"
)
declare -a answers

for question in "${questions[@]}"; do
    clear
    echo "${question}"
    read tmp
    answers+=("$tmp")
done

TITLE=$(node ./bin/newBook.js -t "${answers[0]}" -a "${answers[1]}" -p "${answers[2]}")

clear
git checkout -b read/${TITLE}
git add .
git commit -m"Files created for ${TITLE}"
git status
