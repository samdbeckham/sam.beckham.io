import fs from "fs";
import { program } from "commander";

program
  .version("1.0.0")
  .option("-t, --title <title>", "book title")
  .option("-a, --author <author>", "book author")
  .option("-p, --purchaseLink <purchaseLink>", "book purchase link")
  .parse(process.argv);

createPost();

function createPost() {
  const { title, author, purchaseLink } = program.opts();
  const date = new Date();
  const dir = "./src/content/read";
  const formattedDate = date.toISOString().split("T")[0];
  const filename = title
    .toLowerCase()
    .replaceAll(/[^a-zA-Z0-9\- ]/gim, "")
    .replaceAll(" ", "-");
  const path = `${dir}/${filename}.md`;
  const data = `---
date: ${formattedDate}
title: ${title}
author: ${author}
cover: ${filename}.jpg
purchase_link: ${purchaseLink}
background: "#ff00ff"
reading: true
---

`;
  fs.writeFile(path, data, () => {
    // We log this here so we can pass it to the bash Script.
    // Should probably chain this properly, but heh
    console.log(filename);
  });
}
