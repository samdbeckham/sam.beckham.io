import fs from "fs";
import yamlFront from "yaml-front-matter";
import OpenAI from "openai";

const POST_DIR = `./src/content/wrote`;
const EMBEDDINGS_FILE = `./src/data/embeddings.json`;

const client = new OpenAI({ apiKey: process.env.OPEN_AI_TOKEN });

async function getPostData() {
  const filenames = fs.readdirSync(POST_DIR);
  const posts = await Promise.all(
    filenames.map(async (filename) => {
      const data = await fs.readFileSync(`${POST_DIR}/${filename}`);
      const frontMatter = yamlFront.loadFront(data);
      return {
        filename,
        title: frontMatter.title,
        tags: frontMatter.tags,
      };
    })
  );
  return posts;
}

async function getEmbeddings(input) {
  const response = await client.embeddings.create({
    model: "text-embedding-ada-002",
    input,
  });
  return response.data;
}

async function makeTheEmbeddingFile() {
  const postData = await getPostData();
  if (!postData) {
    console.warn("no post data");
    return;
  }
  const embeddablePostData = postData.map(
    (post) => `${post.title}: ${post.tags.join(", ")}`
  );
  const embeddings = await getEmbeddings(embeddablePostData);
  if (!embeddings) {
    console.warn("no embeddings");
    return;
  }
  const hydratedPostData = embeddings.map((embeddingData, i) => ({
    slug: postData[i].filename.replace(".md", ""),
    embedding: embeddingData.embedding,
  }));

  fs.writeFile(
    EMBEDDINGS_FILE,
    JSON.stringify(hydratedPostData).replaceAll("},{", "},\n{"),
    () => {}
  );
}

makeTheEmbeddingFile();
