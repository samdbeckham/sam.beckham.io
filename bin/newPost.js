import fs from "fs";
import { program } from "commander";

program
  .version("1.0.0")
  .option("-t, --title <title>", "post title")
  .parse(process.argv);

createPost();

function createPost() {
  const { title } = program.opts();
  const date = new Date();
  const postDir = "./src/content/wrote";
  const imgDir = "./src/assets/images/articles";
  const formattedDate = date.toISOString().split("T")[0];
  const filename = title
    .toLowerCase()
    .replaceAll(/[^a-zA-Z0-9\- ]/gim, "")
    .replaceAll(" ", "-");
  const imgPath = `${imgDir}/${filename}`;
  const postPath = `${postDir}/${filename}.md`;
  const data = `---
title: "${title}"
date: ${formattedDate}
description: ""
background: "#ff00ff"
tags:
  - engineering
---

`;
  fs.writeFile(postPath, data, () => {
    // We log this here so we can pass it to the bash Script.
    // Should probably chain this properly, but heh
    console.log(filename);
  });
  fs.mkdir(imgPath, 0o0777, () => {
    // console.log(`image directory created ${imgPath}`);
  });
}
