import fs from "fs";

const dummyData = [
  {
    wm_property: "mention-of",
    slug: "all-meetings-are-optional",
    id: "12345",
    author: {
      name: "Nick",
      photo: "http://placecage.com/200/200",
      url: "#",
    },
    content: {
      text: "I want to take his face, off.",
    },
    published: "Thu, 05 Sep 2024 08:10:00 GMT",
    url: "#",
    wm_target: "bees",
  },
];

const FILE_PATH = "./src/data/webmentions.json";
const API_ORIGIN = "https://webmention.io/api/mentions.jf2";
const { GRIDSOME_WEBMENTIONS_TOKEN } = process.env;

async function run() {
  const webmentions = await fetchWebmentions();
  const sortedMentions = webmentions.sort(
    (a, b) => new Date(a.published) - new Date(b.published)
  );
  fs.writeFile(FILE_PATH, JSON.stringify(sortedMentions), () => {});
}

async function fetchWebmentions(page = 1) {
  // TODO: Paginate
  const url = new URL(API_ORIGIN);
  const limit = 20;
  url.searchParams.set("token", GRIDSOME_WEBMENTIONS_TOKEN);
  url.searchParams.set("page", page);
  url.searchParams.set("per-page", limit);

  const response = await fetch(url);
  const mentions = await response.json();

  const parsedMentions = mentions.children
    .filter(
      (mention) =>
        mention["wm-property"] === "in-reply-to" ||
        mention["wm-property"] === "mention-of"
    )
    .map((mention) => ({
      target: mention["wm-target"],
      id: mention["wm-id"],
      author: mention["author"],
      text: mention["content"]?.text,
      published: mention["published"],
      url: mention["url"],
    }));

  if (mentions.children.length === limit) {
    return parsedMentions.concat(await fetchWebmentions(page + 1));
  }
  return parsedMentions;
}

run();
