module.exports = {
  globDirectory: "public/",
  globPatterns: ["**/*.{html,css,js,png,json}"],
  swDest: "public/service-worker.js",
  cleanupOutdatedCaches: true,
  cacheId: "sambeckham",
  skipWaiting: true,
};
