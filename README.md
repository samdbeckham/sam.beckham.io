# Can I get a README?

## Getting the project running

Make sure you're using Node 16.0.0. If you're using `asdf` this should happen automatically.

```
node --version
```

Install all the things with npm:

```
npm install
```

Copy the envfile and set the webmentions token

```
cp .env.example .env
```
