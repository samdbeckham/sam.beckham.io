import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";
import vue from "@astrojs/vue";

const { SITE_BASE, SITE_PATH } = process.env;

// https://astro.build/config
export default defineConfig({
  site: SITE_PATH || "https://localhost:4321",
  base: SITE_BASE || "/",
  integrations: [mdx(), sitemap(), vue({ devtools: true })],
  markdown: {
    shikiConfig: {
      theme: "material-theme-darker",
    },
  },
  outDir: "public",
  publicDir: "static",
});
