import { defineCollection, z } from "astro:content";

const read = defineCollection({
  type: "content",
  // Type-check frontmatter using a schema
  schema: z.object({
    title: z.string(),
    author: z.string(),
    cover: z.string(),
    purchase_link: z.string(),
    reading: z.boolean().optional(),
    date: z.coerce.date(),
    background: z.string().optional(),
    description: z.string(),
  }),
});

const wrote = defineCollection({
  type: "content",
  // Type-check frontmatter using a schema
  schema: z.object({
    title: z.string(),
    date: z.coerce.date(),
    background: z.string().optional(),
    video: z.string().optional(),
    description: z.string(),
  }),
});

export const collections = { wrote, read };
