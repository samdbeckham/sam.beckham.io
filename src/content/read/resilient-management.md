---
date: 2021-08-15
title: Resilient Management
author: Lara Hogan
cover: resilient_management.jpg
purchase_link: https://abookapart.com/products/resilient-management
background: "#E57373"
description: "'Resilient Management' is a valuable resource for managers, offering clear and actionable advice on building trust, managing oneself, and handling work challenges."
---

Every now and again, a book just speaks to you.
This is one of those books.

Lara was always a great source of advice as an engineer, and this continues into management.
The book is full of clear, actionable information that I'd recommend to any manager, at any point in their career.
It reminds us to focus back on the basics and to build a team on a foundation of trust.
I keep returning to it to help me with the various challenges that crop up at work.

There's also plenty of advice on how to manage yourself, your time, and your workload; not just your team.
It weighed in heavily when I [took control of my calendar](/wrote/take-control-of-your-calendar) and the section on building a voltron has been particularly helpful.
