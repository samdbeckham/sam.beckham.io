---
date: 2021-03-05
title: Crucial Conversations
author: Kerry Patterson
cover: crucial-conversations.jpg
purchase_link: https://amzn.to/3Bp9OvP
background: "#E53935"
description: "'Crucial Conversations' is a guide to confidently navigate difficult conversations with varying opinions and strong emotions."
---

Crucial Conversations is a guide to handling difficult conversations with confidence, especially when stakes are high, opinions vary, and emotions run strong.

The book offers tools to prepare for high-impact situations with a six-minute mastery technique, make it safe to talk about almost anything, be persuasive and not abrasive, keep listening when others blow up or clam up, and turn crucial conversations into the action and results you want. The book is useful for high-stakes conversations at work or at home with neighbors or spouses, and it teaches readers to communicate effectively in order to achieve positive resolutions. The skills learned in this book will enable readers to handle crucial conversations with greater ease and achieve better outcomes.
