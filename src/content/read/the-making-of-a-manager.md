---
date: 2020-07-05
title: The Making of a Manager
author: Julie Zhuo
cover: making_of_a_manager.jpg
purchase_link: https://amzn.to/3LGqLXf
background: "#4DB6AC"
description: "'The Making of a Manager' is highly recommended for new managers. It offers valuable insights and relatable stories that prepare you for the challenges of a managerial role, particularly in engineering."
---

A must-read for any new or aspiring managers.

This book helped me more than any others in the run-up to taking on a managerial role.
It helped me learn what to expect, and the story-telling nature of it really gelled with me.

I re-read it a couple times early in my management career and found myself nodding along with a, "Yep, that's exactly what happened".
Julie really nailed the thoughts and feelings of a new (engineering) manager.
