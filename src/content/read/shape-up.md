---
date: 2022-04-18
title: Shape Up
author: Ryan Singer
cover: shape-up.png
purchase_link: https://basecamp-goods.com/products/shapeup
background: "#FBC02D"
description: "'Shape Up' is a product development guide by Ryan Singer from Basecamp. It offers processes like six-week cycles and scope management, addressing challenges faced by software companies as they grow."
---

Shape Up is a guide to product development at Basecamp.

It presents the specific processes used by the company to make progress on its products, including six-week cycles and the techniques used to define and manage the scope of work. The book also provides readers with better language to deal with the challenges, risks, and uncertainties that come with product development. The book’s author, Ryan Singer, highlights some common challenges that software companies face as they grow, such as difficulty in finding time to think strategically, projects that go on and on with no end in sight, and struggles to deliver features as quickly as in the early days. Basecamp encountered these challenges as they grew from four people to over 50. Singer believes that the techniques in this book are applicable to anyone involved in product development, including founders, CTOs, product managers, designers, and developers.
