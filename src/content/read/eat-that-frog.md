---
date: 2019-12-20
title: Eat That Frog
author: Brian Tracey
cover: eat-that-frog.jpg
purchase_link: https://amzn.to/42xkPqg
background: "#C0CA33"
description: "'Eat That Frog!' by Brian Tracy is a time management guide emphasizing prioritization, organization, and tackling the most challenging tasks first."
---

In "Eat That Frog!", author Brian Tracy offers a guide to effective time management, urging readers to focus on the most important tasks and learn to organize their day.

The book provides a simple metaphor of eating a live frog first thing in the morning as a way of tackling the most challenging task of the day, the one that is most likely to be procrastinated on, but can have the greatest positive impact on one's life. Tracy emphasizes decision-making, discipline, and determination as vital components of effective time management. The book also includes new information on how to prevent technology from taking over one's time, and offers 21 practical steps to help readers stop procrastinating and get important tasks done.
