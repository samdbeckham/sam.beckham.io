---
date: 2021-02-05
title: The Five Dysfunctions of a Team
author: Patrick Lencioni
cover: the-five-dysfunctions-of-a-team.jpg
purchase_link: https://amzn.to/3ppyDVl
background: "#C62828"
description: "'The Five Dysfunctions of a Team' is a leadership fable by Patrick Lencioni that explores team challenges."
---

"The Five Dysfunctions of a Team" is a leadership fable that explores the complex world of teams.

The story follows Kathryn Petersen, the CEO of Decision Tech, as she faces the challenge of uniting a team in disarray. Throughout the book, author Patrick Lencioni outlines the five dysfunctions that often plague teams, which he believes can be overcome through a powerful model and actionable steps. Lencioni's story serves as a reminder that leadership requires courage as well as insight. The book offers a timeless lesson for team leaders, providing a compelling fable with a deceptively simple message. Whether you're a new leader looking to build a strong team or an experienced one striving to improve, "The Five Dysfunctions of a Team" offers insights that can help you overcome common hurdles and build a cohesive, effective team.
