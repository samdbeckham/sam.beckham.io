---
date: 2015-03-28
title: Remote
author: David Heinemeier Hansson and Jason Fried
cover: remote.png
purchase_link: https://amzn.to/3LLKJj7
background: "#F44336"
description: "'Remote' advocates for remote work, providing advice on successfully working remotely, emphasizing flexibility, technology, and eliminating commuting."
---

"Remote" advocates for remote work, arguing that the traditional "under one roof" model of work is no longer necessary.

The authors, who work at tech company 37signals, argue that remote work leads to increased productivity and innovation. The book provides advice for managers, individuals, and teams on how to successfully work remotely. It includes chapters such as "Talent isn't bound by the hubs", "It's the technology, stupid", and "The virtual water cooler". The book argues that working remotely allows for more flexibility and eliminates the daily grind of commuting and meetings. The authors' goal is to end the "tyranny" of being confined to an office and to promote the benefits of remote work.
