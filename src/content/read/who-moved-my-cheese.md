---
date: 2020-05-09
title: Who Moved My Cheese
author: Spencer Johnson
cover: who-moved-my-cheese.jpg
purchase_link: https://amzn.to/3pj43wS
background: "#FBC02D"
description: "'Who Moved My Cheese?' is a parable about four characters looking for cheese in a maze, representing what we want in life. It teaches readers to adapt to and enjoy change."
---

"Who Moved My Cheese?" is a simple parable that reveals profound truths.

The book is the story of four characters who live in a maze and look for cheese to nourish them and make them happy. Cheese is a metaphor for what you want to have in life. The maze is where you look for what you want, perhaps the organization you work in, or the family or community you live in. In the story, the characters are faced with unexpected change in their search for the cheese. One of them eventually deals with change successfully and writes what he has learned on the maze walls for you to discover. The book teaches readers how to anticipate, adapt to, and enjoy change and be ready to change quickly whenever they need to. Written for all ages, this story takes less than an hour to read, but its unique insights will last a lifetime.
