---
date: 2015-08-12
title: How to Talk About Books You Haven't Read
author: Pierre Bayard
cover: how-to-talk-about-books-you-havent-read.jpg
purchase_link: https://amzn.to/3nGKH4g
background: "#795548"
description: "In 'How to Talk About Books You Haven't Read,' Pierre Bayard argues that being cultured doesn't require reading, but understanding a book's place in culture."
---

In "How to Talk About Books You Haven't Read," Pierre Bayard argues that being a truly cultured person in today's age of infinite publication doesn't necessarily require having read a book, but rather understanding its place in our culture.

Bayard explores different kinds of "non-reading," such as forgotten books, unknown books, and books we've skimmed briefly, and the challenging situations in which we may need to discuss our reading with others. Drawing on examples from works by famous authors, Bayard encourages readers to consider what reading means, how we absorb books as part of ourselves, and why we spend so much time talking about what we have, or haven't, read.
