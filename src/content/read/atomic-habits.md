---
date: 2023-06-11
title: Atomic Habits
author: James Clear
cover: atomic-habits.jpg
purchase_link: https://amzn.to/3J69kyG
background: "#FFA726"
reading: false
description: "Building better habits does not require doing everything all at once."
---

This book has had more impact on my life than any other.
Its broad scope makes it applicable to almost anything.
I sleep better, my writing habits have improved significantly, I started a whole new note-taking system that is changing the way I read, and I read a lot more than before.

The core idea is that [small changes](/wrote/starting-small) over time add up to significant results.
Building better habits does not require doing everything all at once.
Instead, strive to be 1% better or to do 1% more each day.
Over time, the results stack up like compound interest.
Once a habit is established, it becomes second nature and therefore easier.

This concept can be applied to almost anything, making it incredibly powerful.
The four stages of habit building outlined in [this article](https://jamesclear.com/three-steps-habit-change) can help make developing these habits even easier.

I wholeheartedly recommend this book to anyone who wants to improve, even if it's just a little bit each day.

For those who want to take the concept of Atomic Habits even further, the author's website (<https://jamesclear.com/>) has a wealth of resources.
There are articles, videos, and even a free newsletter that provides tips and strategies for building better habits.
Additionally, the author offers an online course for those who want to dive deeper into the topic of habit formation.
