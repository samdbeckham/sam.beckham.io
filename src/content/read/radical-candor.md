---
date: 2021-05-22
title: Radical Candor
author: Kim Scott
cover: radical-candor.jpg
purchase_link: https://amzn.to/3VGHRsD
background: "#F57C00"
description: "Radical Candor is a management approach that balances aggression and empathy. The book by Kim Scott emphasizes personalization, productivity, and purpose."
---

Radical Candor is a management approach that strikes a balance between being overly aggressive and overly empathetic with employees.

It involves providing guidance through a mix of praise and criticism to produce better results and help employees develop their skills. Kim Scott, the author of the book, identifies three principles for building better relationships with employees: making it personal, getting things done, and understanding why it matters. The book offers actionable lessons for both bosses and those who manage them, drawing on the author's first-hand experience. It shows how to be successful as a boss while maintaining integrity and humanity. Radical Candor is recommended for those who want to create a positive work environment where employees love their work, their colleagues, and are motivated to succeed.
