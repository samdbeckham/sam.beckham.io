---
date: 2012-09-08
title: The Design of Everyday Things
author: Donald Norman
cover: the-design-of-everyday-things.jpg
purchase_link: https://amzn.to/3NQ8edK
background: "#37474F"
description: "'The Design of Everyday Things' is a transformative book emphasizing the significance of good design for human use, from physical objects to software."
---

"The Design of Everyday Things" is a must-read for anyone who designs anything for human use, whether physical objects or computer programs.

It is equally valuable for anyone who uses anything created by humans. The book has the potential to change how we experience and interact with our physical surroundings, by highlighting the importance of good design and showing us the flaws of bad design. It could also raise our expectations about how things should be designed.
