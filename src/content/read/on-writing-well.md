---
date: 2016-04-12
title: On Writing Well
author: William Zinssner
cover: on-writing-well.jpg
purchase_link: https://amzn.to/3HOV1Ot
background: "#03A9F4"
description: "'On Writing Well' is a valuable book for those who want to improve writing skills, offering insights from a distinguished writer. It's praised for clarity and sold over a million copies."
---

"On Writing Well" is a book that provides sound advice on how to write clearly and effectively.

It is recommended for anyone who wants to improve their writing skills, whether it's for personal or professional use. The book covers a wide range of topics, including writing about people, places, science and technology, business, sports, and the arts. It also offers insights from a distinguished writer and teacher and provides fundamental principles for writing. The book has been praised for its clarity, warmth of style, and has sold over a million copies, making it a valuable resource for anyone who wants to improve their writing.
