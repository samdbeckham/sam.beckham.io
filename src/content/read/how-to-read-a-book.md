---
date: 2015-09-17
title: How to Read a Book
author: Mortimer J Adler
cover: how-to-read-a-book.jpg
purchase_link: https://amzn.to/3NQUm2P
background: "#00BCD4"
description: "'How to Read a Book' is a classic guide published in 1940 that teaches effective reading techniques, including judging books, critical reading, and extracting the author's message."
---

"How to Read a Book", originally published in 1940, is a classic guide to reading effectively.

It introduces the different levels of reading, from elementary to speed reading, and teaches readers how to achieve them. The book explains how to judge a book by its cover, read critically, and extract the author's message. It also provides guidance on the techniques that work best for different genres, including practical books, imaginative literature, plays, poetry, history, science and mathematics, philosophy, and social science works. The authors offer a recommended reading list and reading tests to measure progress in reading skills, comprehension, and speed. "How to Read a Book" is an essential resource for anyone who wants to become a better reader.
