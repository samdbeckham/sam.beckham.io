---
date: 2023-01-28
title: Engineering Management for the Rest of Us
author: Sarah Drasner
cover: engineering-management-for-the-rest-of-us.jpg
purchase_link: https://amzn.to/42Btsjz
background: "#5C6BC0"
description: "'Engineering Management for the Rest of Us' helps managers in engineering who lack formal training in management to collaborate and work towards common goals."
---

Sarah recognizes that many managers in the field of engineering management were originally promoted from engineering positions without much formal training in management.

The book aims to help these managers develop skills for collaborating with teams and working towards common goals. Sarah notes that there are fewer resources available on engineering management than on programming, likely because people and their processes are non-deterministic and working relationships are complex. The book covers a range of topics from feedback to project management, and is intended for managers as well as individual contributors who want to better understand the concerns of the business. Ultimately, Sarah hopes to help managers develop the skills necessary to support their teams and create healthy working environments.
