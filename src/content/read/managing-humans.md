---
date: 2023-06-30
title: Managing Humans
author: Michael Lopp
cover: managing-humans.jpg
purchase_link: https://amzn.to/3CQKT4W
background: "#FBC02D"
reading: true
description: "I've been following Rand's work for a long time. This book has been on my to-read list for far too long. Let's change that."
---

I've been following Rand's work for a long time. This book has been on my to-read list for far too long. Let's change that.
