---
date: 2022-05-08
title: It Doesn’t Have to Be Crazy at Work
author: David Heinemeier Hansson and Jason Fried
cover: it-doesnt-have-to-be-crazy-at-work.jpg
purchase_link: https://amzn.to/3NKahjs
background: "#C62828"
description: "In 'It Doesn't Have to Be Crazy at Work,' the authors advocate for a calmer approach to business, emphasizing productivity through reduced waste and distractions."
---

In "It Doesn't Have to Be Crazy at Work," the authors argue that long hours and a chaotic work environment are not necessary for success in business.

They propose a new approach, "the calm company," that directly addresses the stress and anxiety that affect many workplaces. The authors assert that long hours, an excessive workload, and a lack of sleep are not marks of productivity, but rather of stupidity. They suggest that the key to better productivity is to reduce waste and distractions, rather than increasing hours worked. Fried and Hansson provide evidence to support their argument and draw on their company's twenty-year experience of a "calm" culture at Basecamp. This book is a practical guide for managers and executives of all industries and company sizes who seek to implement a more productive and healthier work environment.
