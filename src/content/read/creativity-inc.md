---
date: 2019-09-03
title: Creativity Inc
author: Ed Catmull
cover: creativity-inc.jpg
purchase_link: https://amzn.to/3VGrYlT
background: "#C62828"
description: "'Creativity Inc.' is a book by Ed Catmull, the founder of Pixar, detailing the company's history and techniques that made it successful."
---

"Creativity Inc." is a part-autobiography, part-history of Pixar, and part-business book that tells the story behind the company that changed animation forever.

Ed Catmull, the founder of Pixar, reveals the ideas and techniques that have made Pixar one of the most widely admired creative businesses and one of the most profitable. The book narrates how, against all odds, Toy Story was created, which started a new generation of animation. Through its focus on storytelling, inventive plots, and emotional authenticity, Pixar revolutionized how animated films were created. "Creativity Inc." is a manual for managers who want to lead their employees to new heights and for anyone who strives for originality, with behind-the-scenes examples from Pixar. It is a book about how to build and sustain a creative culture with a unique identity and through this story, readers learn what creativity really is.
