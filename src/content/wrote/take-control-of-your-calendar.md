---
title: "Take Control of Your Calendar"
date: 2022-02-25
description: "You own your calendar, don't let your calendar own you."
background: "#F67C7C"
tags:
  - async weeks
  - remote work
  - meeting habits
  - calendar management
  - focus time
---

I love [async weeks](/wrote/all-meetings-are-optional/), but they're a temporary fix.

They're the crash diet of the remote work world.
Sure you lose the weight, but you gain it all back once you return to your usual routine.
The healthier solution for the long-term is to change your meeting (or eating) habits.

I wanted to own my calendar and have more time to focus without feeling like I need to resort to an async week.
I'll still have async weeks—we've committed to doing one per quarter—but I'd rather not _need_ them.

## Analyzing the Calendar

The first step in cleaning up my calendar was to assess the current state.
When starting this process, my calendar looked like this:

![My chaotic calendar before I started taking control](@/assets/images/articles/take-control-of-your-calendar/calendar_before.svg)

I've removed the text to keep things confidential, but this is my actual calendar from a few weeks ago.
It's a mess.
There's a lot of meetings and they're all over the place.
Some of those meetings were important and un-missable.
Some of them weren't important at all and my non-attendance wasn't even noticed.
But they all looked the same.

In Lara Hogan's essential book, [Resilient Management](https://resilient-management.com/) she states:

> "I began color-coding my calendar events based on the kind of brain I was going to be using during that time." — Lara Hogan

This exercise is helpful to see where you spend your time.
You can also glance at your calendar and get a feel for the kind of day you're going to have.

I colored my calendar based on the following mapping:

1. <code style="background-color: #d50000">Red</code> – Interviews
2. <code style="background-color: #8e24aa">Violet</code> – 1:1 with reports/manager
3. <code style="background-color: #7986cb">Indigo</code> – 1:1 with peers
4. <code style="background-color: #039be5">Blue</code> – Group meetings I'm leading
5. <code style="background-color: #0b8043">Green</code> – Group meetings I'm not leading
6. <code style="background-color: #f6bf26">Yellow</code> – Optional group meetings
7. <code style="background-color: #f4511e">Orange</code> – Coffee chats
8. <code style="background-color: #a79b8e">Gray</code> – New events (default color)

![Color-coding my calendar](@/assets/images/articles/take-control-of-your-calendar/calendar_colored.svg)

This is a much more useful view.
It's still a mess, but a well-labelled mess.
I can more clearly see where I'm spending my time.

Looking at the week in retrospect with the colors applied, I got a few bits of information:

### 1. I had a few meetings too close to my lunch break

There's a couple of occasions where meetings sandwiched my lunch break.
This usually resulted in me skipping lunch as I get wrapped up in taking notes, writing follow-ups, and prepping for the afternoon.
If I did manage to get lunch, I often had to sneak mouthfuls in during the next meeting.
That's not a good look.

### 2. I worked a 12 hour day on Thursday

I shouldn't do that.
I was a zombie in the latter half of that day and for most of the next one.
This was down to poor management of my interview schedule.
To accommodate candidates in APAC (Asia-Pacific) timezones, I opened my calendar up earlier on Tuesday and Thursday.
However, I didn't adjust the meetings in the latter part of those days to counteract this.
Tuesday was a 10 hour day for similar reasons.

### 3. I spent 30% of my time in meetings, but it felt like much more

This is due to the spread of meetings.
The largest block of uninterrupted work (excluding Friday) was 3 hours.
After that, there's a couple blocks of 2 hours and the rest was 30 minutes here and there.
That's not enough time to settle in and focus on deep-work.

### 4. There were a few meetings I didn't respond to

Most of these were company-wide meetings that I didn't attend, or plan to.
They were optional and I'm sure my presence or response wasn't missed, but that's besides the point.
It made it a lot harder to see the meetings I did have as they were often overlapping and taking up valuable space.

## Fixing the issues

These observations gave me a good starting point to set up some rules to follow.
They needed to be simple and memorable, as they will dictate how I organize my calendar going forward.

### 1. Longer lunch breaks

I won't actually take a 2-hour lunch break.
But by blocking out 2 hours each day for lunch, I've given myself a lot more flexibility.
It's scheduled in my calendar every day and no one can add a meeting on top of it.
It also prevents me sandwiching my lunch between two meetings and skipping lunch as a result.
Plus, if I need to take extra time on the odd day, I can do that.

### 2. No meetings in the morning

Most of the folks I work with are in AMER (North, Central and South America).
Our overlap is in my afternoon so I can schedule all my meetings after lunch.
This gives me all morning, every day, to get my focus work done and dedicate my afternoon to meetings.

There are some exceptions.
Some of the folks I work with are in APAC and I'll have to meet with them earlier.
Bunching them all on the same day will maximize focus time.

### 3. Group like-meetings together

Another benefit of color-coding the events is to highlight context-switching.
In [Resilient Management](https://resilient-management.com/), Lara recommends:

> "Try grouping meetings together to avoid context-switching, or scatter particularly draining events throughout your week." — Lara Hogan

Using the colors I applied earlier makes this a lot easier.
Grouping events of the same (or similar) color together reduces context-switching.

### 4. Say "No" to meetings I'm not attending

This is common courtesy, but it also helps clean out the visual clutter from my calendar.
Especially when paired with setting my calendar to hide declined events.

### 5. No meetings on Fridays

I already had this rule.
In-fact, it's a general rule at GitLab.
We call them [Focus Fridays](https://about.gitlab.com/handbook/communication/#focus-fridays) and they're awesome.
I love this rule, let's keep it.

---

After applying these rules, and doing a bit of calendar shuffling, my calendar for this week looked like this:

![My, much tidier, calendar for this week](@/assets/images/articles/take-control-of-your-calendar/calendar_after.svg)

It's stunning.
Clear, focused mornings; walled off with a hefty block of OOO for a more flexible lunch.
Followed by an afternoon of contextually-grouped meetings, with an easy start and end to the week.

There's 12.5 hours of meetings in there.
This is down ~35% from the 19 hours of meetings in the other week.
10 of those hours are due to me not currently interviewing.
The remaining 6.5 is from declining meetings I don't need to attend.

There's still a few things I'd like to iterate and improve on.
I bunched my APAC meetings together, but they still go outside my usual working hours.
They're also on a day were I have meetings right up until the end of my usual hours.
This means that instead of being able to shift my day forward an hour on those days, I end out working that extra hour.
This isn't a huge problem, but if I can sort it, I should.

There's also a bit of clutter on Thursday.
The checkered meetings are ones I've responded with, "maybe".
They're group meetings where I'll check the agenda 10 minutes before and see if it's worth tuning into or not.
I should be more decisive here, but I'll see how it goes.
At least I've responded with something on these events.
I'm getting there.

## Keeping it up

New meetings are colored gray to make them easy to spot.
As soon as I see these, I should triage them.
I need to respond, assign them a color, and consider if it fits my rules.
If a meeting doesn't fit the rules, I should determine if I'm willing to make an exception or request we move it.

As with everything, this is an ever-evolving process.
I will continue to experiment with my scheduling if something doesn't feel right.

With increased focus-time and less context switching, I'm ready to make every week (almost) as productive as an async week.
I'd recommend you try it out and see how this works for you.

Remember; you own your calendar, don't let your calendar own you.
