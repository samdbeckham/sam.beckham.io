---
title: "Talk: Lessons from Leadership"
date: 2023-07-04
description: "This week, I gave the first talk with no code in the slides. It was all about the lessons I learned from leadership during my time as Acting Chief of Staff to the CTO at GitLab. Unfortunately, it wasn't recorded. But here are my slides accompanied with a rough transcript."
background: "#F44336"
tags:
  - Leadership
  - Chief of Staff
  - Communication
  - Efficiency
  - Lessons
---

This week, I gave my first talk without code in the slides.
It was about the lessons I learned from leadership during my time as [Acting Chief of Staff to the CTO](/wrote/acting-chief-of-staff-to-the-chief-technology-officer) at GitLab.

Unfortunately, it wasn't recorded.
But ([as promised](https://twitter.com/samdbeckham/status/1674698246445309953)) here are my slides accompanied by a rough transcript.
I'll skip the bits where I introduce myself, and you can find the interactive version of the slides [on Slid.es](https://slides.com/samdbeckham/acosttcto).

This concludes my three-part series on my time as the Acting Chief of Staff to the CTO.
Check out [part one](/wrote/acting-chief-of-staff-to-the-chief-technology-officer.md) and [part two](/wrote/chief-of-staff-28-days-later.md) for more context.
Now, let's introduce to the stage… Sam Beckham!

---

![Slide 1: Acting Chief of Staff to the CTO title slide](@/assets/images/articles/lessons-from-leadership/acosttcto1024_1.png)
Hey everyone!
I'm here to talk about my time as the Acting Chief of Staff to the CTO at GitLab.
What this role was, the lessons I learned, and (more importantly) how we can apply those lessons to be more effective in our own roles.

![Slide 5: GitLab](@/assets/images/articles/lessons-from-leadership/acosttcto1024_5.png)
I work at GitLab.
There's somewhere between two and three thousand employees at the company and every single one of us is remote.
For those of you that don't know, GitLab is, "The single application for the entire DevSecOps toolchain".
Another way to think about it is as an open sourced version of GitHub, Snyk, Jira, and all the apps you use to write, plan, and produce code, all in one package.
That's a little wordy, which is why we say, "The single application for the entire DevSecOps toolchain".

My usual role at GitLab is that of an Engineering Manager.
I have seven engineers that report to me.
We all work for the foundations team, which covers Navigation, Settings and our Design system.
But we're here to talk about my time as…

![Slide 6: Acting Chief of Staff to the CTO](@/assets/images/articles/lessons-from-leadership/acosttcto1024_6.png)
The Acting Chief of Staff to the CTO.
This is an unusual title, and not one I've seen anywhere else so let's break it down and dig into what it means.

![Slide 7: Acting](@/assets/images/articles/lessons-from-leadership/acosttcto1024_7.png)
Firstly, "Acting".
Put simply, this means "Temporary".
It was a three-month long rotation where I would leave my usual duties behind and pick them back up once I got back.
Whilst my job title changed for that period, my manager and everything else remained the same.

![Slide 8: Chief of Staff](@/assets/images/articles/lessons-from-leadership/acosttcto1024_8.png)
The main part of this title is, "Chief of Staff".
This is a common role in Government and the most famous usage is the [White House Chief of Staff](https://en.wikipedia.org/wiki/White_House_Chief_of_Staff).
Whilst the responsibilities of these roles are very different, the idea remains the same.
A Chief of Staff is not an administrative or assistive role.
You're not managing calendars, taking minutes, or running errands.
It's a strategic role designed to help with some of the day-to-day duties of the person you're, "staffing" for.
In this case, that person is…

![Slide 9: To the CTO](@/assets/images/articles/lessons-from-leadership/acosttcto1024_9.png)
The CTO.
Not all Chiefs of Staff have this provision as it's usually a role reserved for the top of the organization.
For the White House Chief of staff, this is the President.
For GitLab, this is our CEO, Sid.
Sid has his own Chief of Staff team, lead by our Chief of Staff, Stella.

This role was unrelated to the work they did, and was specifically tailored for our CTO, Eric.

![Slide 11: Chief of Staff](@/assets/images/articles/lessons-from-leadership/acosttcto1024_11.png)
Since the full title of this role is quite cumbersome, I'll refer to it as, "Chief of Staff" for the rest of the talk.
It's not technically true, but it's a lot easier to say.

![Slide 13: Responsibilities and Org Chart](@/assets/images/articles/lessons-from-leadership/acosttcto1024_13.png)
The main responsibilities of the role were:

Holding [one to ones](/wrote/one-to-one-meetings) with The CTO and all the VPs that reported to them.
There was; Christopher (the VP of Engineering), Bartek (the VP of Incubation), Mek (The VP of Quality), Christie (The VP of UX), Steve (The VP of Infrastructure), Tom (Tthe VP of Support), and Johnathan (the VP of Security).
We used these meetings as a touch point so I could hear the needs of the VPs and stay up-to date with what they were working on.
I also used this as an opportunity to learn as much as I could from them.
I'll always be grateful to how open they all were to a random engineering manager asking them questions.

It wasn't all chit-chat though, I had to ensure the OKRs were all on track and understand why some of them may not be.
OKRs are too big of a topic to go into now, but they're essentially shared company goals.
The CTO would have their OKRs which would feed into the CEOs OKRs.
The VPs would each have their own OKRs that fed into the CTOs.
These cascaded down through their directors, managers, and all the way to the individual groups.
Every change at the group level would bubble up through the OKRs and count towards the score.
If an OKR was behind at the CTO level, we could dive into all the OKRs that fed into it to see where the problem was occourring.
Once we found that problem we would…

Foster discussions between people and departments.
Security may need to know what infrastructure was planning, UX might need engineering support, or something else.
I needed to make sure the right people were talking to each other at the right times.

During these conversations we would be thinking about the next quarter's OKRs.
What should our focus be for the next three months?
How can we set ourselves up to achieve these results?
This would largely be driven by the E-Group, but it was important to listen to the members in the department and see what themes arose.

When finalizing these OKRs, it helped to have some sync discussions.
To do this, we organized an offsite to get all the leaders together.
We had a week of discussions and meetings with the team and various other leaders from across the company.
There was also some team bonding activities as there had been quite a shake up about halfway through the quarter…

Eric Johnson announced he was leaving his post as CTO.
He'd been with GitLab for 5 years and had grown the engineering department 8-fold during his tenure.
He was well known and respected by many of us at GitLab.

Stepping in to the Acting CTO role was Ashley Kramer.
She was (and continues to be) GitLab's Chief Marketing Officer and Chief Strategy Officer.
I'm still in awe at how well she handled three C-level roles all at once.
She'll be CEO of somewhere great one day.
Watch this space.

At this point, my priorities shifted somewhat.
I did what I could to aid this transition and help the next Chief of Staff prepare onboarding materials for whoever our next CTO would be.
I still had all the other responsibilities, but this became priority one.

![Slide 14: What I learned](@/assets/images/articles/lessons-from-leadership/acosttcto1024_14.png)
But I'm not here to regale you with stories, or tell you about what I did.
I'm here to tell you what I learned, how I apply it to my regular role, and (hopefully) how you can too.
I've broken it down in to six sections that (not coincidentally) map to the values we have at GitLab.

These values are very important at GitLab, but especially important in senior leadership.
By living these values, leadership sets the precedence for the rest of the company.

![Slide 15: The six sections](@/assets/images/articles/lessons-from-leadership/acosttcto1024_15.png)
These sections are:

1. Collaborate Effectively
2. Drive Results
3. Be Efficient
4. Include Everyone
5. Iterate Continually
6. Be Transparent by Default

We'll dig into all six of these shortly, but I want to point out that all these lessons teach us one, unified thing…

![Slide 16: Communication is Critical](@/assets/images/articles/lessons-from-leadership/acosttcto1024_16.png)
Communication is critical.
If you get the communication right, everything else becomes much easier.
The success of decisions were closely linked to the communication surrounding them.

One thing Eric taught me fairly early on was that…

![Slide 18: For a message to be heard, it should be repeated three times, in three different places, and in three different ways.](@/assets/images/articles/lessons-from-leadership/acosttcto1024_18.png)

For a message to be heard, it should be repeated three times, in three different places, and in three different ways.
Three is an arbitrary number here, but point he was making was that you had to over-communicate.
He also said, "If you don't feel silly repeating yourself, you're not doing it enough".

So how do we communicate at GitLab?
What specific lessons did I learn?

![Slide 19: Use a single source of truth](@/assets/images/articles/lessons-from-leadership/acosttcto1024_19.png)

Use a single source of truth.
At GitLab, this is [our company handbook](https://about.gitlab.com/handbook/).
It's a static site that holds all the company information.
If you want to know how a certain team works, it's in the handbook.
If you need to check the parental leave policy, it's in the handbook.
If you want to see the engineering OKRs for the quarter, they're in the handbook.
Every bit of information you would need is in one single source of truth, our company handbook.

The handbook is a public git repository hosted on GitLab.
So if you want to use Google to search the handbook, you can do that.
You can use git tooling to see who added a line to the handbook, and when.
You can even dig into that change and find the Merge Request (what we call Pull Requests) and the discussions surrounding it.

Whilst a public facing static site may be overkill for your organization, there are smaller alternatives.
Use a shared Notion database, a forum, an intranet, or whatever suits your needs.
The important thing is that it's a single source of truth that's readily available and easily searchable.
If you let your employees self-serve to find this information, it's much better for them and it's much better for you.

![Slide 20: Announce Everywhere](@/assets/images/articles/lessons-from-leadership/acosttcto1024_20.png)
The downside to the handbook is that it's simply a directory of information.
It can be hard to know what changed and which of those changes you should pay attention to.
This is where the more standard forms of communication come back into play.

Remember, for a message to be heard, it should be repeated three times, in three different places, in three different ways.
We would announce things in our company slack channels.
There are general channels like, `#whats_happening_at_gitlab` and `#company_fyi`.
There are more specific channels like `#engineering`, `#ux`, or `#g_manage_foundations_engineering`.
We would post, and cross reference to whatever channels made sense for the message.
They key thing being that we would always link back to that single source of truth, the company handbook.
That's where the messaging was, and that's where it would continue to evolve over time.

We would also use the standard report chain style of getting the message out.
The CTO would tell their VPs, they would tell their reports, they would tell theirs and so on, all the way to the ICs.
This method is usually prone to the telephone effect.
If a detail is slightly changed or missed, the subsequent messages will propagate that change.
By the time you go through several layers, the resulting messages can be quite different.
By linking to a single source of truth, you bypass this entirely.
The message that's being told is that there's been an update and here's how it affects them and their team, followed with a link to the handbook change.

There's another, more crucial, part to communication that I'm missing out here.
That comes a little later.
Let's move on to results.

![Slide 21: Measure Results](@/assets/images/articles/lessons-from-leadership/acosttcto1024_21.png)
How do you know you're achieving results if you don't measure them?
This sounds simple, but it's often neglected.
At GitLab we measure things in a load of different ways.
The most prominent one is our OKRs.
As I mentioned before, these are our quarterly company goals.
We put them front-and-center and check them continually.
There's an entire other talk I could dedicate just to OKRs.
When done correctly, they're a really powerful tool.

We also use KPIs (Key Performance Indicators) to track the more ephemeral aspects of the company.
Number of security issues, % of women in leadership roles, or MR rates.
By keeping an eye on these trends we can ensure we're staying on target and not setting up a load of spinning plates, only to neglect them long enough to have them crash back down to the ground.
Something to keep in mind when tracking these is to…

![Slide 22: Opposing Metrics](@/assets/images/articles/lessons-from-leadership/acosttcto1024_22.png)
Measure opposing metrics.
If we look at MR rate as a metric, it's easy to see how tracking this alone could go quite wrong.
The MR rate is the number of Merge Requests merged, per engineer, per month.
This is averaged out across the departments so we're never looking at the rate of an individual engineer (that's a whole other rabbit hole).
This metric essentially tracks how much code we're shipping each month.

The problem with tracking this on its own is that it encourages velocity at the cost of everything else.
The easiest way to ship more code, is to care less about the quality of that code.
That's why we have KPIs for performance, security issues, bugs, and usability too.
They ensure that we continue to ship consistently, but not at the expense of quality.

The same goes for hiring rate vs turnover, or diversity vs pay equality, and there are likely parallels in your business too.
Measure opposing metrics to avoid side-effects.

![Slide 23: Start with a decision](@/assets/images/articles/lessons-from-leadership/acosttcto1024_23.png)
Onto Efficiency now, and this is a lesson we can all apply.
It's a simple trick that works wonders in getting things moving.
Start with a decision.
If you're about to enter a discussion on the best way to solve a problem, it's good to come prepared with potential solutions, but it's better to come prepared with a decision.
Instead of saying, "To solve this problem we could do X, Y, or Z", modify it slightly to say, "We will use X To solve this problem. We have also looked into Y and Z".

This subtle change does a few things.
Firstly, you have your decision from the start.
If you spend the entire discussion going round in circles, you still have your decision.
Secondly, if someone is strongly against X or knows of another, better option, they are much more likely to speak up if X has already been decided.
Finally, if the person you're talking to doesn't need to know the details, and just wants a decision, you're already there.

This only works if this decision is a strong opinion, weakly held.
Don't defend the decision so hard that you shut down all discussion, that's counterintuitive.
If the discussion leads to Y, Z, or something else being the better choice, great!
It might be a different decision, but you have a decision and that's what's important.

![Slide 24: Be a multiplier](@/assets/images/articles/lessons-from-leadership/acosttcto1024_24.png)
As your scope of influence increases, it's far more efficient to be a multiplier than an adder.
Every task you do yourself has an additive effect to the product, service, or company you work for.
Everything you do to increase the efficiency of those around you, has a multiplicative effect.
For the CTO, it was far more efficient to increase the productivity of their reports by 1% than it would be to try to increase their own productivity by 100%.

Focus on moving things out of the way for your team and helping them become more effective.
With a lower scope of influence, being a multiplier is still more efficient, just at a smaller scale.
Be a multiplier.

![Slide 25: Shut up and listen](@/assets/images/articles/lessons-from-leadership/acosttcto1024_25.png)
I understand the irony of this slide, 20 minutes in to a presentation where I'm the only one speaking.
This is the slide I mentioned earlier that addressed the far more important side of communication, listening.

Encourage discussion, encourage feedback, encourage people questioning your decisions.
Then be sure to actively listen to that discord, and show that you're hearing it.
We did this in a few ways, the main two being the engineering week in review, and the CTO office hours.
Though this is just as much about defining a culture than it is defining a process.

The week in review is an async document where anyone from engineering could announce things that happened during the week.
It's a living document that gets a new set of headings each week and everyone is encouraged to participate.
Either by adding an announcement, or reading through to see what else is going on in the department.

The CTO office hours are a more sync approach.
Every week or so, we hold a meeting where the CTO would welcome topics and discussion from everyone in engineering.
This could be a discussion on a recent change, getting more information on the why behind a decision, or raising an issue that the CTO should know about.
Contrarian topics were encouraged, and always heard.

You'll be surprised what you hear when you shut up and listen.

![Slide 26: Include everyone](@/assets/images/articles/lessons-from-leadership/acosttcto1024_26.png)
This goes hand-in-hand with listening.
Don't put yourself in a bubble, listen to and include everyone.
Include the engineers, include the designers, include managers and ICs.
Include people from different backgrounds, with different ideals, and ideas.
Include people regardless of their position on the globe, or within the company.
Include everyone.

We do this at GitLab in many ways.
One of my favorites is the way we do meetings.
Every meeting has an agenda that the whole company can see, read, and add to.
If you can't make a meeting, you can check the agenda, add your points and have them verbalized by someone else.
You can even go back and watch the meeting because most meetings are recorded, and hosted for the whole company to see.
Some of them even make their way to YouTube.
Where we can, we rotate meetings to cover different timezones each week so different people can benefit from the sync portion of the meeting.

Include everyone, and make them feel heard.
This is THE most important part of communication.

![Slide 27: Ship and iterate](@/assets/images/articles/lessons-from-leadership/acosttcto1024_27.png)
This one should be familiar to a lot of you agile folks.
Ship an MVC, then iterate on it to improve it.
It's been the way most of us write code for a long time and it works.

Senior leaders don't tend to write code, but that doesn't stop them iterating.
Instead of iterating on products, they iterate on decisions and processes.

A lot of what I mentioned earlier becomes easier if you iterate.
Let's say you made a change to the handbook and communicated it widely.
If you're fostering the right listening environment, you will likely receive some feedback on this change.
What better way to show you're listening than to iterate on that decision and take that feedback into account.
Just don't turn 180˚ every time someone dislikes your decision.
That's not efficient and would be absolute chaos.
Feedback is a gift, but you need to unwrap it the right way.

![Slide 28: Don't wait for perfection](@/assets/images/articles/lessons-from-leadership/acosttcto1024_28.png)
It's much easier to iterate if you hold off on perfection.
Good enough is good enough, you can always refine it later.
If we wait for something to be perfect, there's a good chance we'll wait too long and miss our opportunity.
This goes for code, decision making, and almost anything else.
Make the call, and improve on it later.

![Slide 29: Work (internally) in the open](@/assets/images/articles/lessons-from-leadership/acosttcto1024_29.png)
This last one is a bit of a tough one, but yields great results.
GitLab have a radically transparent approach to working.
Our handbook is public, our codebase is public, and a good portion of our meetings are too.
The amount of things we have in public has lessened somewhat since we (ironically) went public, but we still do as much in the open as we can.

Being open allows you, and the people you work with, to self-serve more and not have to spend time asking each other for things.
If your policies, roadmaps, and discussions are all out in the open, the chances are that the answers to your questions are too.
Being fully transparent helps with hiring too.
We can send the candidate links to the right handbook pages to read up on our values, how we work, and what the team is working on.
This makes the sync time in the interview more effective.
The usual questions you get asked at the end of an interview are often replaced with far more interesting ones because they already know what you're working on and how you work.
They read it in the handbook.

As I said, this one can be tough, especially if your whole company isn't behind this idea.
I don't recommend recording and publishing meetings on YouTube if your company doesn't already do this.
That sounds like a data protection breach waiting to happen.
However, you can still work _internally_ in the open.
Make sure your team have all the resources they need to self-serve and can catch up on what happened whilst they were out without needing to rely on a summary from someone else.
Consider setting up an internal handbook as your single source of truth.
Make meeting agendas visible to everyone in the company, and open up your calendar so people can find them.
Do one small thing to be more transparent and iterate from there.

![Slide 30: This slide intentionally left blank](@/assets/images/articles/lessons-from-leadership/acosttcto1024_30.png)

Those are the high-level learnings from my time as Chief of Staff, and my continued time at GitLab.
Not all of these will work for you, in your company or in your role.
But if you can take one thing from this and use it to be 1% more effective then I've been a good multiplier today.

![Slide 31: Lessons learned](@/assets/images/articles/lessons-from-leadership/acosttcto1024_31.png)
In summary; Collaborate effectively, Drive results, Be efficient, Include everyone, Iterate continually, and Work in the open.
If you do all this you can communicate better and be a more effective person, regardless of your role.

Communication is crucial.
If you get the communication right, everything else becomes much easier.
Being heard is not enough, you need to practice the more important half of communication and know when to shut up and listen.
But for when you do need to be heard, remember…

![Slide 32: For a message to be heard, it should be repeated three times, in three different places, and in three different ways.](@/assets/images/articles/lessons-from-leadership/acosttcto1024_32.png)
For a message to be heard, it should be repeated three times, in three different places, and in three different ways.

![Slide 33: Thank you](@/assets/images/articles/lessons-from-leadership/acosttcto1024_33.png)
I've been Sam Beckham, you've been fantastic.
Thank you for listening.

---

I want to say an overdue, thank you to everyone I worked with during this rotation.

Thank you to Eric for creating this role and giving me this opportunity.
It's an unusual role, but I got a lot from it and I am both grateful and humbled for the opportunity.

Thank you to all the VPs for welcoming me to the team and teaching me those lessons.
Seeing how each of you dealt with the varying challenges your departments faced was a far better lesson that any textbook or talk could give.

Thank you to Ashley, for stepping in and stepping up so quickly.
You juggled three c-level roles with such ease it was staggering.

Thank you to Marisa, our EBA, for all your work on the offsite and for being a great touch point through this whole thing.

Thank you to my team members in Verify, for remaining awesome during my time away.
I never did come back, but y'all are doing just fine without me.

Finally, thank you to GitLab, for supporting these programs and continuing to be a great place to work and grow.
