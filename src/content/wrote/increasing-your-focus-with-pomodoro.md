---
title: 'Increase Your Focus with Pomodoro'
date: 2022-06-24
description: "The Pomodoro technique isn't novel, or complicated. It's simple, and it works. Here's how it helps me focus."
background: "#e0192b"
tags:
  - Productivity
  - Time Management
  - Focus Technique
  - Pomodoro Technique
  - Distraction Management
---

The Pomodoro technique isn't novel, or complicated.
It's simple, and it works.
Here's how it helps me focus.

## What is the Pomodoro technique?

The beauty of the Pomodoro technique is in its simplicity.
You can sum it up in one sentence:

> "Work on one thing (and one thing only) for 25 minutes, then have a five minute break."

There's [a little more to it](https://francescocirillo.com/pages/pomodoro-technique), but that one sentence gets you 80% of the way there.
The key is to force yourself to focus on **a single task** for the full 25 minutes.
No checking email, no responding to slack messages, no scrolling Twitter, nothing.
Focus on that one task and that one task only.

It's surprising how much you get done by having sole focus on one task for a set amount of time.
Spending precious brain power on context-switching slows us down.

After 25 minutes, take a 5 minute break.
Not just a break from the task, but a proper break.
Go make a cup of tea, stand outside, or get up and walk about.
It's like a rest period at the gym.
Do whatever helps your brain recover.

It's recommended to take a longer, 25 minute break after 3 or 4 rounds of Pomodoro.
These focus sessions can feel intense.
Don't burn yourself out.

## It's not all-or-nothing

If you're inclined to do so, you can use the Pomodoro technique all day.
Chunk your whole day into 25 minute focus sessions and work through each one.
For some folks, this works wonders.
For others, it doesn't.
As I have [mentioned before](/wrote/find-what-makes-you-productive), everyone is different and your results may vary.

I use it more sporadically.
It's a tool I use when I want to really focus on one particular task.

For example, I know I want to spend a couple of hours working on this post.
So I'll dedicate four Pomodoro sessions to writing it.
Two for the first draft, one for editing it, and one for publishing and making final tweaks.
These won't all be back-to-back though.
I like to have some time between the first draft and the edit to come at it fresh.
Between the second and third Pomodoro, I'll work on something else.

## Time-box the time-sinks

This technique not only helps you focus, but it helps to time-box certain activities.
In writing this post, I know I only have the appetite to spend a couple hours on it.
I have other things to do and don't want to spend all day on it.
Time-boxing it to four Pomodoro sessions ensures I don't go down a rabbit-hole and while away at it all day.
Knowing my own mind, I would start on this in the morning and before I know it I'd be looking up and it's the end of the day.
Anyone who's played [Civilization](https://www.destructoid.com/civilization-beyond-earth-makes-weekends-disappear/) will know exactly what I'm talking about.
Having a 25-minute timer helps to pull me out of my own head.

There it goes now, see you in five!

---

If you're struggling to get this working for you, these tips might help:

### 1. Do not disturb

Turn on "do not disturb", put your phone in the other room, or use a focus app.
Do whatever you need to do to put a barrier between you and your distractions.
These notifications will still be there when your 25 minutes is up.
Don't let the pings, bleeps, and bloops distract you from what you're focusing on.

### 2. Block out time on your calendar

Block some time on your calendar for "focus time".
This lets people know not to bother you during that time.
Or, at the very least, tells them exactly why you're not responding to their messages.
[Take control of your own time](/wrote/take-control-of-your-calendar).

### 3. Use a physical timer

It's called the Pomodoro technique because traditionally, it uses a Pomodoro timer.
These are small kitchen timers in the shape of a tomato.
Or, [if you're Italian](https://translate.google.com/?sl=it&tl=en&text=pomodoro&op=translate), a Pomodoro.

Sure there's a load of Pomodoro apps, but most require you to have your phone with you.
See point 1 for why this is a bad idea.
Plus, it's kinda fun to have a physical timer.

![A traditional, physical Pomodoro timer](@/assets/images/articles/increasing-your-focus-with-pomodoro/timer.jpg)
_[Egg timer in tomato shape for Pomodoro Technique](https://www.flickr.com/photos/149561324@N03/37941061684) by [Marco Verch](http://foto.wuestenigel.com/?utm_source=37941061684&utm_campaign=FlickrDescription&utm_medium=link), licensed under [CC by 2.0](https://creativecommons.org/licenses/by/2.0/)_

### 4. Distracted? Start again

Find yourself distracted during your focus period?
Maybe you let an email notification pull you in, or a passing thought took hold.
Start the timer again.
Whether you're 2 minutes in or 20.
Start the timer again.
This helps annoy you into focusing next time and not fall back into old habits.

### 5. Don't take it too serious

Counter to all the advice above, maybe the best way to get this to work for you is to care less.
We're all different and for some people the pressure of being forced to focus is all they can focus on.
There are no rules here; [do what works for you](/wrote/find-what-makes-you-productive).

## Give it a go

Search for this technique and you'll find hundreds of Pomodoro apps.
Don't bother with them.
The beauty of this technique is its simplicity.
Use what you have, find a task to focus on and set a timer for 25 minutes.
Grab that old egg-timer out of the drawer, use your watch, or yell, "Alexa! Set a timer for 25 minutes!".
Whatever gets you started.

As ever, I'm interested to hear your thoughts.
Have you done this before and loved it?
Did you hate it?
Are you eager to try it?
Give it a go and join the discussion below.

What's stopping you?
