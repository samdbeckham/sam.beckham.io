---
date: 2021-10-05
title: Automate Your Meeting Agendas
background: "#eb9532"
video: "gqFbZi8Hyoc"
description: "If you use Google Docs for your meeting or 1:1 agendas, here's a way to automate them. No more copy/pasting agenda templates."
tags:
  - Google docs
  - Meeting agendas
  - Automation
  - Google scripts
  - Engineering management
---

I've wrestled with far too many Google docs, for far too many meeting agendas, for far too long.
There must be an easier way.

After a year or so in engineering management, I've run a lot of meetings.
Every meeting should have an agenda, and every agenda should be filled ahead of time.
For repeating meetings, I was copy-pasting agenda templates every week.
Because this task is manual and I'm a lowly human, I often forgot to do it and frantically added the agenda 10 minutes before the meeting.
It was a tedious, repetitive, and boring task; something computers are great at.

We use Google docs for our meeting agendas so everyone can contribute in real-time.
This also means we can use Google scripts to introduce some automation.
So that's exactly what I did.
Better yet, I created a library so you can use it too.

## How to use it

You can add this to any Google doc and it should work fine.
The only thing you need to have in your document is a horizontal line separating the heading from the agenda section.
If the code won't run, this is likely the reason.

Open the document you want to add the script to, then go to `tools > Script editor`.
This should load up the Google Script window and let you start entering some code.
Before we start writing anything, we'll want to add the `MeetingUtils` library.
Click on the `+` sign next to `Libraries` and enter the ID: `1ODNAyDAGqdLk1yfrkXOpoBLvO8OVvxie-xzCToiestMx8uBpCnaSjQD9`.

After that, it's as simple as adding some variables, creating the function, and setting up a schedule.

For a 1:1 meeting, use the `nextOneToOne()` function.
It takes the name of your report, your name, and the day of the meeting.
This day is numeric where `sunday = 0`, but you can use the constants included in the library to make it a little easier to read.
See the [full range of options](#options) at the bottom of the post.

```javascript
function addNextAgenda() {
  MeetingUtils.nextOneToOne({
    reportName: "Fred",
    managerName: "Sam",
    meetingDay: MeetingUtils.getConstants().WEDNESDAY,
  });
}
```

For a group meeting (or similar) use the `nextGroupMeeting()` function.

```javascript
function addNextAgenda() {
  MeetingUtils.nextGroupMeeting({
    meetingDay: MeetingUtils.getConstants().WEDNESDAY,
    sections: ["Announcements", "Discussion"], // You can add as many sections as you like here
  });
}
```

Once that's done, make sure your function can run by selecting the `addNextAgenda` function (or whatever you named it) and hitting, "run".
You may need to do the permissions dance to give the script access to edit your document.
Now, whenever you run that function, it creates a meeting agenda item for the next selected day.

This is only semi-automated right now.
To fully automate this, we need to create a schedule that runs every week.
Head over to the `triggers` tab on the far left and add a repeating trigger.
You can set it to run on all sorts of events, but the most useful one is the time-driven week timer.
I like to set mine to run at midnight the night after each meeting.
This way there's almost a full week for people to add their agenda items.
Do what works for you though.

That's all you need to automate this boring task and get back to doing the interesting things.

---

## Options

There are a few different options you can use.
The defaults will work fine, but with a few tweaks, this script is much more versatile.

### `nextOneToOne(OPTIONS)`

By default, this generates a typical 1:1 agenda in the format:

```markdown
## YYYY-MM-DD

### Report Name

1.

### Manager Name

1.
```

```markdown
| Option                 | Description                                                                                                                                  | Required | Default |
| ---------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------- |
| `reportName`           | The name of the report                                                                                                                       | `true`   | -       |
| `managerName`          | The name of the manager                                                                                                                      | `true`   | -       |
| `meetingDay`           | The day of the meeting in numerical format where `sunday = 0`. The script uses this to determine when the next occurrence of the meeting is. | `false`  | `0`     |
| `useHumanReadableDate` | When `true`, changes the date format from `2021-10-11` to `October 11th 2021`                                                                | `false`  | `false` |
| `questionOfTheDay`     | When `true`, adds a randomly selected question of the day to each agenda. This is still experimental.                                        | `false`  | `false` |
```

### `nextGroupMeeting(OPTIONS)`

This is a less-tailored version of the 1:1 template, generating an agenda item in the format:

```markdown
## YYYY-MM-DD

### Section 1

1.

### Section 2

1.
```

```markdown
| Option                 | Description                                                                                                                                 | Required | Default                           |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------------------------- |
| `meetingDay`           | The day of the meeting in numerical format where `sunday = 0`. The script uses this to determine when the next occournce of the meeting is. | `false`  | `0`                               |
| `sections`             | An array of all the sections you want to include in the agenda. There's no limit to this.                                                   | `false`  | `["Announcements", "Discussion"]` |
| `useHumanReadableDate` | When `true`, changes the date format from `2021-10-11` to `October 11th 2021`                                                               | `false`  | `false`                           |
```
