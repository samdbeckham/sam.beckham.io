---
title: "Acting Chief of Staff to the Chief Technology Officer"
date: 2022-08-01
description: "Acting Who to the what now?"
background: "#1565C0"
tags:
  - Acting Chief of Staff
  - Executive role
  - Leadership
  - Career growth
  - Learning opportunity
---

Today, I take on a new role.
A role with an expiry date.
A role that has me working with the highest levels of engineering leadership at GitLab.
A role with far too many words in the title.
From August 1st till November 1st, I'll be the [Acting Chief of Staff _to_ the CTO](https://about.gitlab.com/job-families/engineering/acting-cos-to-the-cto/).

## What is an Acting Chief of Staff to the CTO?

It's an unusual title.
It reminds me of Dwight Schrute being the "Assistant _to_ the Regional Manager".
Let's dissect it and talk about what it means.

A CoS (Chief of Staff) is often used in government and military, but has increased in popularity in the tech industry.
They work with an executive (usually the CEO) to help them be more effective.
This could be running meetings, advancing initiatives, solving disputes, etc.
It depends on the skills of the CoS and the needs of the executive.

A good way to think of it is like the [Hand of the King](https://gameofthrones.fandom.com/wiki/Hand_of_the_King) from Game of Thrones.
Hopefully there's a few less beheadings, but the general idea is the same.
A CoS is an adviser, liaison, and can (in some cases) make decisions on the executive's behalf.

The CoS role is usually to the CEO of a company.
We have a permanent CoS ([Stella](https://gitlab.com/streas)) for our CEO ([Sid](https://gitlab.com/sytses)).
However, this particular role is, "…to the CTO…" and supports our Chief Technology Officer, Eric.
Otherwise, everything else is the same.

Another key difference is that this is an _"acting"_ role.
I won't be "acting" in a traditional sense.
In this instance, acting means, "temporary".
I'll walk like a CoS, talk like a CoS, and do all the things a CoS does.
But on paper, I'll still be an Engineering Manager.
After three months, I'll go right back to my old role.

## Why am I taking this role?

This role gives me insight into the upper levels of the company, and I get to see how a C-level executive operates.
It helps me see if this level of management is where I want to end up.
It allows me to build empathy for upper management and see how and why they make decisions.
The role is quite new, so I get to help shape it, and grow it for the next folks.

The temporary nature of this role offers me a level of reassurance too.
All being well, I'll have spent three months in one of the best learning opportunities to come my way.
If not, I'll have lost nothing but time.

## I have no idea what I'm doing

Once again, I've found myself in a role where [I have no idea what I'm doing](/wrote/i-have-no-idea-what-im-doing).
But that's where I thrive.
I'm far more comfortable outside my comfort zone than I am within it.
I would encourage you to step out your comfort zone and [do something that scares you](/wrote/public-speaking-is-scary).

As I usually do, I'll learn and share all I can.
If you're a Chief of Staff yourself, I want to hear from you.
If you know someone who is, please put them in touch.
I only get three months at this, but I want to give it a good run.
