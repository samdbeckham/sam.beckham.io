---
title: "One to One Meetings"
date: 2023-06-09
description: "Discover the power of one-on-one meetings for engineering managers. Learn valuable tips, including the importance of customized approaches, prioritizing your team's agenda, and avoiding status reports. Get a free one-on-one template to enhance your meetings and improve communication with your reports. Build stronger relationships, foster career growth, and optimize your management style."
background: "#FFB300"
tags:
  - leadership
  - one-on-one meetings
  - relationship building
  - management tips
  - career growth
---

One to ones are the single most important meetings a leader will have with their reports.
Here's some tips (and a free one to one template)

## No One to One Fits All

One to ones are your main tool for building relationships with your reports.
Like all relationships no two are the same.
Every relationship has different needs at different times.
For that reason, there's no one-size-fits-all when it comes to one to ones.
Every manager, and every report, will have different needs from their one to one.
The secret to finding those needs is iteration.

## It's not your meeting

This is **_their_ meeting, not _yours_**.

Spend at least the first half of the meeting talking about whatever they want to talk about.
This could be an issue they're facing, a discussion about their career, or what they got up to at the weekend.
If you have something you need to talk with them about, that's fine.
You'll have time in the second half of the meeting.

Remember, this is **_their_ meeting, not _yours_**.

If they need to reschedule it for that week, let them.
If the scheduled time doesn't work for them anymore, move it.
Better yet, give them the ability to do it themselves and operate a tell-don't-ask policy.
"Tell me you're moving the meeting, don't ask me if you can."

Because, this is **_their_ meeting, not _yours_**.

Don't let it devolve into a status report.
If they want to update you on where they're at and discuss potential blockers, that's fine.
When a one to one turns into a weekly status report, this is usually because it's what they think you want to hear, not because it's what they want to talk about.
Try to steer the conversations away from status updates if you can.

## One to One template

Even though it is their meeting, and each meeting is different, starting with a good framework is key.
Here's the template I use:

<a href="https://docs.google.com/document/d/11ieq3d_um05a_Gb_vTQOw5KknAW8jqmEmG_zEU6tjzA/edit?usp=drive_link" target="_blank" class="button button--full">📄 One to One Template</a>

You can download it in any format and update it to suit your needs.
If you copy the doc to your drive, you get my my [automated meeting agenda script](/wrote/automate-your-meeting-agendas/) included.

We split the agenda into two sections; sticky info above the break, and weekly agendas below.
Let's break them down.

Right at the top is the title.
It's nothing special, but the order of the names is deliberate.
Their name first, then yours.
This helps set the tone and further emphasize that (say it with me) this is **_their_ meeting, not _yours_**.

Below that are some useful links.
In this template, there's a link to the meeting room and a brag document.
These will be different for every report.
They can include important issues, promotion documents, training, or whatever you'll reference often.
Remember to [keep the important things within reach](/wrote/keep-the-important-things-within-reach).

The table below that are the focus areas.
They're set up to keep the three main areas of focus front-of-mind.
They can be performance goals, but I'd recommend decoupling performance targets from these.
I use them as areas we both need to focus on to help them with their career growth.
Learning a new topic, preparing for a promotion, keeping an eye on work/life balance, etc.
By keeping them at the top of the document, you're less likely to forget them.
Check in with them regularly, and if there's ever a lull in communication, they're a great ice breaker.

Next up is action items.
This is where I like to store any promises we made to each other during the meeting.
It's great for ephemeral tasks that like outside the usual issue/ticketing system.
At the start of the next meeting, take a quick look at these and make sure you're keeping your promises to each other.
Keep each other accountable for this.

Below the line break is the bread-and-butter of one to ones, the agenda.
Make sure to have next week's agenda ready to go.
When a thought pops into your mind during the week, you can drop it into the agenda to discuss at the next meeting.
You can do this at the end of each meeting, or you can [automate it](/wrote/automate-your-meeting-agendas) if you're using Google docs.
New entries get added at the top so you have a record of all the past meetings for reference.
Be sure to take notes as you go.
This can be a tough habit to get into (and something I'm working on myself) but your future self will thank you for it.

## Informal by default, formal when it matters

Try to keep it informal by default.
By having a formal structure, you can have informal conversations and still get things done.
Sometimes conversations need to get more serious, but if you make them informal by default you'll both get a lot more out of them.
One to ones are your best tool for building relationships, so use them to do that.

What do you like to include in your one to one documents?
Let me know on [Twitter](https://twitter.com/intent/tweet?url=https%3A%2F%2Fsam.beckham.io%2Fwrote%2Fone-to-one-meetings)!
