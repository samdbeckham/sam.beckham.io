---
date: 2019-10-02
title: Running the JAMstack on GitLab
background: "rgb(56, 52, 121)"
description: "The JAMstack has piqued my interest. Not because it's fast, secure, or totally hot right now. But because it got me thinking, \"We could run that on GitLab\""
tags:
  - JAMstack
  - GitLab
  - static site generator
  - GraphQL
  - serverless functions
---

The JAMstack has piqued my interest. Not because it's fast, secure, or totally hot right now. But because it got me thinking, "We could run that on GitLab".

These thoughts cross my mind a lot. GitLab is such a versatile tool that it has endless applications. I use GitLab to run a conference, keep track of my wedding plans, and even help create GitLab. It's almost definitely a case of [Maslow's Hammer](https://en.wikipedia.org/wiki/Law_of_the_instrument) where, _"…everything looks like a nail"_. But who cares, I'm eyeing up a JAMstack shaped nail right now, with my GitLab branded hammer in hand.

## What is the JAMstack?

Before we dig into what I did, let's explain what the JAMstack is. The acronym itself stands for **J**avascript, **A**PIs, and **M**arkup. Which on its own, could describe almost any website. Here's a quote from the top of [https://jamstack.org/](https://jamstack.org/) that explains it better:

> A modern architecture —  
> Create fast and secure sites and dynamic apps with **J**avaScript, **A**PIs, and prerendered **M**arkup, served without web servers.

The key part here is **"served without web servers"**. JAMstack sites are static. They do all the compiling at build time, not at runtime. It's often confused with isomorphic websites built with [Next](https://nextjs.org/), [Nuxt](https://nuxtjs.org/), or similar. But the concept is entirely different. We're not rendering pages on the server, then hydrating them once they get to the frontend. We're building the pages once (at build time) then serving up those static pages at runtime. This makes for a blazing fast website with little to no server costs.

## Let's do this

If you hadn't already guessed, the project I created on the JAMstack is the blog you're reading right now. It looks and behaves in much the same way that it did previously, it's just a lot faster, and easier to update.

The first thing I needed to do was pick a static site generator. I had a look at [Gatsby](https://www.gatsbyjs.org/) and [Eleventy](https://www.11ty.io/), but ultimately settled on [Gridsome](https://gridsome.org/). It's based on [Vue.js](https://vuejs.org/) and integrates with [GraphQL](https://graphql.org/), which is something I've been wanting to look into for a while. This seemed like the perfect excuse.

With the site generator chosen, I needed to work out where to store my data. When this site was running on Jekyll, I used markdown files stored in the repo. Whilst this worked well enough, I wanted a little more flexibility. Something that required a CMS. I chose [Sanity](https://www.sanity.io/) based on a recommendation from [Jamie Bradley](https://twitter.com/jamiebradley234). Sanity is a headless CMS with a GraphQL output layer, a perfect fit for my needs.

With those two chosen, I had everything I needed to get started developing locally. I moved over all my old posts to Sanity, which was a lot harder than I expected. I wish there was an easy way to convert markdown to Sanity blocks (Sanity's rich text editor) but I had to do it all manually. I guess the one upside to my low output on this blog is that there weren't too many posts to migrate.

I already had a lot of the components of my blog written in Vue from when I tried to move it over to [VuePress](https://vuepress.vuejs.org/). A Vue based static site generator that ended out being unfit for purpose. I copied these components over, tweaked them to fit, and added my GraphQL layer. Then I was good to go.

Once I was ready to deploy, I copied the example [`.gitlab-ci`](https://gridsome.org/docs/deploy-to-gitlab/) file from Gridsome's documentation and pushed the site to GitLab. This triggered the build process on GitLab's CI pipeline and built out the static site to GitLab pages. After two painless DNS additions, I had my domain hooked up to GitLab pages with [automatic SSL certs](https://about.gitlab.com/2019/07/22/gitlab-12-1-released/#automatic-https-for-pages-with-lets-encrypt).

With very little set up—which consisted more of copy/pasting than any real hard work—I had my blog up-and-running. When I pushed a change to the `master` branch, GitLab's CI pipeline would trigger a build, regenerate all the static files, and re-deploy my website. This was the boring solution. The smallest possible change I could make to get my blog migrated to the JAMstack with GitLab.

## Iterating on the Boring Solution

The boring solution was great, but are were a few small tweaks I could do make to make the process a little nicer.

Because my content was in Sanity, GitLab wasn't aware of any changes I made to it. The content publised on the website would be the content from Sanity at the time of the most recent build. I could manually trigger the pipeline after I published a change to pull in the latest data, but I'm far too lazy for that. I needed to automate this. Thankfully, GitLab allows you to create [pipeline triggers](https://docs.gitlab.com/ee/ci/triggers/) that can listen to a webhook and start a pipeline every time it's used. On the Sanity side of things, you can [call that webhook](https://www.sanity.io/docs/data-store/webhooks#adding-webhook) every time you publish a post. Now, whenever I publish a post, it triggers the pipeline then the website is re-built and deployed. It's not as instant as a usual CMS set up but it only takes a minute or so, and it only has to happen once.

I'm also a bit of a stickler for performance. The boring solution was fast, but still fell down on a few points. I've fixed them all now and it's blazing fast. I'll save the story of how I did that for another day. To summarise, I minify, lazy-load, pre-fetch, and cache all of the things. I also added in a service worker for good measure and that sweet [offline first](https://sam.beckham.io/wrote/greased-lightning) experience.

## Serverless functions

Whilst this site has no need for serverless functions (Functions as a Service, FaaS), they are a large part of the JAMstack. Static websites are great for sites with little user interaction beyond reading information. But what if your site needed to be more complex? What if you needed to allow users to log in, make purchases, or post content? This is where serverless functions come in. They're API endpoints that (when called) spin up a VM, run some code, then disappear again until they're needed next time. There are a lot of different platforms that offer this service. AWS Lambda is the biggest right now, but there's also Azure Functions, Google Cloud Functions, and a load of other providers that offer functions as a service. All of these integrate nicely with [GitLab Serverless](https://about.gitlab.com/2018/12/11/introducing-gitlab-serverless/) should the need arise.

If you want a more in-depth look at how FaaS can be used with the JAMstack, take a look at this section of, "[Are you being servered](https://youtu.be/OUbkm4AH9g4?t=1498)" by Phil Hawksworth. Then go back to the beginning and watch the entire talk. It's a great example of what you can do with the JAMstack.

## Have a go yourself

If you want to have a go at building your own JAMstack website on GitLab, I've made [this project](https://gitlab.com/samdbeckham/sam.beckham.io) completely public. You can look at the [code](https://gitlab.com/samdbeckham/sam.beckham.io/tree/master), clone the project yourself, and even watch my [pipelines](https://gitlab.com/samdbeckham/sam.beckham.io/pipelines). If GitLab's not your thing, head on over to [staticgen.com](https://www.staticgen.com/), pick a generator, and get your site up and running in seconds on [Netlify](https://www.netlify.com/).

---

## Edit

Since releasing this post, I decided that Sanity was not for me.
It was an extra dependency that didn't always play nice with my pipelines and due to the nature of the JAMstack, I didn't always know when it wasn't working.
Besides, for what I needed, markdown is fine.

I've removed Sanity and will continue to write my posts in Markdown.
