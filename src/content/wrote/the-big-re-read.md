---
title: "The Big Re-Read"
date: 2023-07-17
description: "In the last six months of 2023, I plan to re-read some of the more impactful management books. Who's with me?"
background: "#D81B60"
tags:
  - Reading
  - Books
  - Self-improvement
  - Management
  - Learning
---

I’ve listened to my favorite songs hundreds of times and watched my favorite movies tens of times. However, most of my favorite books have only been read once. I aim to change that.

Earlier this year, I added a [reading list](/read) to my website. It’s not all the books I’ve ever read, but a curated list of the ones that had the most impact on my life, or career. Each book is accompanied by a brief review and a link to purchase that book. The problem is, I read a lot of these a long time ago, so my memory of them is a little hazy. I’m not proud to admit that Chat GPT helped me write a few of the older reviews, and it shows. I don’t remember enough specifics about each book to give an accurate, personal account of them. But we can change that. There’s some great lessons in these and my note-taking method is much better now. It’s time for a big re-read.

With six months remaining in the year, my plan is to re-read six of the most impactful books and extract as much knowledge from them as possible. I hope that re-reading them will help me notice details and draw on lessons that I may have missed before. Like re-watching a good movie; you begin to notice all the background details.

## The List

<div class="post-list">

[![Managing Humans](/book_covers/managing-humans.jpg)](/read/managing-humans)

[![How to Read a Book](/book_covers/how-to-read-a-book.jpg)](/read/how-to-read-a-book)

[![The Making of a Manager](/book_covers/making_of_a_manager.jpg)](/read/the-making-of-a-manager)

[![Radical Candor](/book_covers/radical-candor.jpg)](/read/radical-candor)

[![Resilient Management](/book_covers/resilient_management.jpg)](/read/resilient-management)

[![Who Moved My Cheese](/book_covers/who-moved-my-cheese.jpg)](/read/who-moved-my-cheese)

</div>

### July: [Managing Humans](/read/managing-humans)

This one is a bit of a cheat. I haven’t actually read this one before. I’m currently reading it though and I’m keen to finish it. Whilst I’ve not actually read it, it feels very familiar as I’ve been following the [Rands leadership blog](https://randsinrepose.com/), [podcast](https://randsinrepose.com/the-important-thing/), and [slack instance](https://randsinrepose.com/welcome-to-rands-leadership-slack/) for a long time. This book has been on my to-read pile for longer than I care to admit.

### August: [How to Read a Book](/read/how-to-read-a-book)

We’re going right back to basics with the most meta book in my collection, “How to Read a Book”. It’s quite a dry read and is fairly dated (first published in 1940) but the advice in here is solid. It teaches you ways to read books more efficiently so you can get through them faster and retain as much information as possible. Techniques that will come in handy for the remainder of the year.

### September: [The Making of a Manager](/read/the-making-of-a-manager)

This was one of the books I read in my preparation for management. It’s the one that stuck with me the most. Partly because of its excellent advice, and partly because of the fun illustrations. It would be great to go back to this after having some management mileage under my belt to see how much of the advice sunk in.

### October: [Radical Candor](/read/radical-candor)

I love this book. My weakest skill when starting out in management was giving tough feedback. I’ve come a long way since then—thanks in no small part to this book—though I still feel there’s room for improvement. November is the time for annual reviews at GitLab so October is the perfect month to re-read this one.

### November: [Resilient Management](/read/resilient-management)

This book directly influenced my post, “[Take Control of Your Calendar](/wrote/take-control-of-your-calendar)”. and has inspired much of how I manage today. This is one book I've read several times already, but will be happy to read again.

### December: [Who Moved My Cheese](/read/who-moved-my-cheese)

December is often a chaotic month. Which is why I’m giving myself an easy read to cap this re-read off. Don’t misinterpret this though, this book absolutely deserves to be on this list. In management, in engineering, and in all aspects of life; the one constant is change. How well you adapt to that change can make all the difference. There’s a reason this has been one of the best-selling business books of all time for decades.

## Join me

If you want to join-in and read along with me, tell me on [Mastodon](https://mastodon.social/@samdbeckham), or [Twitter](http://twitter.com/samdbeckham) and we can chat about what we learned together. I’ll come back and update this post as I go through.
