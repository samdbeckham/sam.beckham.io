---
title: "Find What Makes You Productive"
date: 2021-11-01
description: "Create and cultivate the productivity workflow that works for you"
background: "#cd3700"
tags:
  - Productivity
  - Task management
  - Methodologies
  - Time management
  - Personal development
---

In my never-ending quest to find the secret of infinite productivity, I've realized one thing:
No one method works for everyone.

I've read the usual heavy-hitters like, [Getting Things Done](https://amzn.to/3mrY70k), [Eat That Frog](https://amzn.to/3Cqyeni), and [The 7 Habits of Highly Effective people](https://amzn.to/3Ctg6cD).
I've listened to hours of podcasts, and talked with folks from all over the place.
I've tried this, that, and the other thing.
My mistake was thinking too prescriptively.

This was all good advice, but they were insights into what worked for the people that wrote them.
I am not those people, and (most likely) neither are you.

The real secret to unlocking your productivity is trying different methods and picking the bits that work for you.
Create your own franken-methodology then continually assess and iterate upon it.
What works for you today may not always work as your life changes, your priorities shift, or you take on different responsibilities.

## My (current) process

My current process is a hodge-podge of other methodologies that work for me now.
It has changed a lot and it will change again.
Use it as inspiration and look deeper and see what works for you.
Build your own productivity monster!

I use [todoist](https://todoist.com) to collate all my tasks in one place.
These come from GitLab issues, emails, conversations, and random thoughts in my own brain.
To maintain my sanity I keep them all in one place; that place is todoist.
The important thing for me is that I can add these tasks fast, from anywhere.
I make sure to [keep it within reach](/wrote/keep-the-important-things-within-reach).

I triage my tasks according to the [1-3-5 rule](https://www.themuse.com/advice/a-better-todo-list-the-135-rule).
I plan for the week and iterate on that plan daily.
I delegate some tasks and abandon the tasks that don't matter.
The remaining tasks get a priority label (`P1`, `P2` or `P3`).

Each day has **only one** `P1` task.
This is my most important task of the day.
I do it first and [Eat That Frog](https://amzn.to/3Cqyeni) before moving on to my other tasks.
If anything unexpected comes up in the afternoon I'll likely have already done the important thing that day.

There are **three** `P2` tasks.
These tasks are likely to get done, but can be pushed along a day or two if needed.
If a task gets pushed along too many times, I will re-asses it.
If it's important, I'll bump it up to a `P1` and make sure to eat it first thing the next morning.
If it's not important, it goes onto the backlog.

Finally, there are **five** `P3` tasks.
These are of similar importance to `P2` tasks but are a lot quicker to do.
If I have a spare few minutes between meetings, or I finish another task early, Ill pick one of these up.

All these tasks are time-boxed with a [pomodoro timer](https://amzn.to/2Zwo5XQ).
This helps me focus on the task at hand and prevents me going too deep into a rabbit-hole on some of the more open ended tasks.

I try to be as transparent as possible with my task-list.
I [created a web app](https://twitter.com/samdbeckham/status/1364192807276212224) that scrapes my todo list for the current day and displays it for ~everyone~ the whole company to see.
The app also helps me communicate my task list in slack using our Geekbot standup.

This method seems clinical and prescriptive.
That's because it is.
In all honesty, I deviate from it more than I stick to it.
But having this plan in place helps me stay on track, even if I need to wander off that track every now and again.

## The Important Thing

The important thing to note is not what I do.
It's that I've taken aspects of all sorts of different methodologies to build one that works _for me_.

I used to get distracted a lot and spend more time on tasks that interested me.
I combatted this using [Eat That Frog](https://amzn.to/3Cqyeni) and the [pomodoro timer](https://amzn.to/2Zwo5XQ).

I used to feel overwhelmed at the amount of tasks I had.
I combatted this by collating them all into one place.

I used to agree to do things verbally, then completely forget about them.
I combatted this by keeping my task-list [within reach](/wrote/keep-the-important-things-within-reach).

There's more I still need to improve on, but I'll do so iteratively.
Experiment, iterate, and find what works for you.
