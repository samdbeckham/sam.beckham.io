---
title: "All Meetings are Optional"
date: 2021-11-26
description: "I started to feel like I couldn't finish the work I needed to do because meetings were getting in the way. So I declined all my meetings for a week and went async."
background: "#00838F"
tags:
  - Meetings
  - Asynchronous communication
  - Remote work
  - Productivity
  - Work-life balance
---

I started to feel like I couldn't finish the work I needed to do because meetings were getting in the way.
So I declined all my meetings for a week and went async.

When doing all-remote, async communication is key.
We have to think about typically synchronous things (like meetings) in a different way.
At GitLab, ~~all~~ most meetings are optional and have an agenda ready ahead-of-time that anyone can edit.
You can attend meetings asynchronously and work on your own schedule.
Turning the usual way of thinking about meetings on its head puts people back on a level playing field.

> "The beauty of asynchronous is that team members can contribute to meetings that occur while they sleep." — GitLab's [All-Remote handbook](https://about.gitlab.com/company/culture/all-remote/asynchronous/#asynchronous-meeting-participation)

Having an [active agenda ahead of time](../automate-your-meeting-agendas) allows people to contribute to the meeting asynchronously.
This has been great for me as it allows me to contribute to meetings that occur outside my working hours.
It allows me to hire and manage folks in APAC (Asia-Pacific) timezones, which I'm [currently doing](https://grnh.se/602d58d62us).
It also allows me to focus on getting things done, without the usual meeting interruptions.
That is exactly what I did last week.
We call it an [async week](https://about.gitlab.com/handbook/marketing/corporate-marketing/all-remote/#async-weeks), and they're fantastic.

I'd still "attend" meetings by adding my points to the agenda and watching the discussions on the recording.
I just don't have to be there at the exact time they were happening.
Recording meetings felt weird at first, but you get used to it.
Being able to watch them at 2x speed and skip the points that don't apply to you are absolute game-changers.

A crucial step with async week is to **let people know you're doing it**.
You'll likely have a few meetings where you'll be missed if you don't show up.
It also prevents people from scheduling meetings with you for that week.
Let people know, and over-communicate your intentions.
I did this with a cross-posted slack message and brought it up in my usual meetings the week before.

![Hey folks! I’m taking an async week to catch up on my backlog and focus on hiring. I’ve declined all my meetings for the week but I’ll still be here if you need me and will be checking all the agendas for updates. If we have to meet, that’s not a problem. Just let me know and I’ll be there. Happy Friday and I’l see y’all on the other side!](@/assets/images/articles/all-meetings-are-optional/slack-message.png)

Next, it's the cathartic process of declining all your meetings.
Clear, calendar, bliss.
You can then [plan your workload](../find-what-makes-you-productive/) with ease and revel in all the free time you've given yourself.

I used this week to burn though my backlog, but it also gave me a new outlook on the meetings I do have.
I couldn't do an async week every week, but there's a few meetings I don't need to attend as often, or at all.
These meetings can pile up.
My next step; get brutal with my regularly scheduled calendar events.
But that's a topic for another day.

This wouldn't work without our company-wide stance of async-first communication.
Huge props to [Darren](https://twitter.com/darrenmurph), [Jessica](https://twitter.com/jessicareeder), and our remote team for helping foster a culture that allows us to take weeks like this.
Check out our [all-remote handbook](https://about.gitlab.com/company/culture/all-remote/asynchronous/) for tips on how to do this.

_**Side note:** I made a desktop wallpaper to remind me that, "all meetings are optional". [Check it out](/downloads/all_meetings.png)_
