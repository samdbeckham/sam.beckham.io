---
title: "Chief of Staff: 28 Days Later"
date: 2022-08-29
description: "I'm a third of the way through my Acting Chief of Staff to the CTO rotation. Here's what I've learned so far."
background: "#FB8C00"
tags:
- Acting Chief of Staff
- GitLab
- Values
- Collaboration
- Results
---

I'm a third of the way through my [Acting Chief of Staff to the CTO](/wrote/acting-chief-of-staff-to-the-chief-technology-officer) rotation.
Here's what I've learned so far.

My day-to-day includes all the tasks listed in [the job description](https://about.gitlab.com/job-families/engineering/acting-cos-to-the-cto/).
But to describe this role as a set of tasks doesn't give it justice.
When asking folks what a Chief of Staff does, I got the typical, "it depends".
This is a very self-driven role and you need to work out how to be effective.
It's been quite freeing, though I do miss my usual team.
Three months is a long time to be away, and not much time to on-board.

## Giving CREDIT to our values

GitLab's core set of [values](https://about.gitlab.com/handbook/values/) drive everything we do.
They spell out, "CREDIT" and stand for; Collaboration, Results, Efficiency, Diversity, Iteration, and Transparency.
We live these values at every level.
They are not just words on a (virtual) wall.

Whilst I look to these values in my regular role, they are much more important at this level of the company.
If leadership expect the rest of the company to live by these values, they need to champion them.

### Collaboration

A key aspect of this role is helping others collaborate.
When one department or team member needs something, help them find where to go to get it.
All whilst remaining as async as possible.
This could be tracking training participants, raising awareness of an initiative, or anything in-between.

One of the tools we have for this is the Engineering Week in Review.
It's an internal newsletter/document that anyone in the company can read or contribute to.
My role is to promote and curate it so the right people see the right messages at the right time.

### Results

Results are right at the top of our [values hierarchy](https://about.gitlab.com/handbook/values/#hierarchy).
With this being more of a supporting role, measuring results can be tricky.
The "acting" aspect of this role requires a bias for action.
You've only got three months in this job so if you want something doing, you had better start soon and move quick.

An example of this is the Engineering off-site.
It's towards the end of the quarter, but requires a lot of planning.
Rather than wait until the last month to start, I started in week two.
I started by blocking out the time on everyone's calendars and iterated from there.
By [starting small](/wrote/starting-small.html), I got the ball rolling and made future collaboration much easier.

Start early and worry about the details later.

### Efficiency

This role only lasts three months, so you need to be as efficient as possible.
[Automation](/wrote/automation) has helped.
I've [automated agendas](/wrote/automate-your-meeting-agendas), created issue templates, and helped set up tracking for some OKRs.
If a computer can do it faster than a human, I'm going to try to automate it.

My 1:1 meetings with the VPs are incredibly efficient.
They're busy folks and sync time with them is expensive.
It pays to be as async as possible and come prepared to these meetings.
I have reoccurring items, focus areas, and actionables as I do with every 1:1.
I just lean on them more and make sure they're up-to date.
If there's nothing to talk about, we skip the meeting.
It's a little different to what I'm used to with 1:1s, but efficiency is key.

### Diversity, Inclusion, and Belonging

Collaboration works best when everyone is included and feels like they belong.
We want to make sure everyone knows that they can voice their thoughts and concerns to the CTO.
Eric (our CTO) is fantastic at this, and I've seen many contrarian questions asked in his office hours.
They're always met with understanding and reason, no matter the topic, or who's asking.
I love this, and want to showcase this more to folks in the company.
I helped with this by re-wording the CTO office hours description with an emphasis on collaboration and inclusion.
If you're a GitLab employee reading this, come join us in the next office hours and share what's on your mind.

### Iteration

I've written about [iteration](/wrote/starting-small) before but it goes doubly so in this role.
If I spend too long thinking about what to do, my tenure will be over and I'll have done nothing.

Organizing the Engineering offsite is one of my tasks as Chief of Staff to the CTO.
It reminds me of [organizing a conference](/wrote/running-a-conference.html), or [starting a meetup](/wrote/starting-a-meetup.md).
Just with much less marketing, and a much shorter time-frame.
The only way to eat this elephant is one bite at a time.
We started by finding a time that worked for all the attendees and blocked it on their calendars.
Then we moved to looking for a city to host it in.
Later we'll look at venues, hotels, flights, and the agenda.

One. Bite. At. A. Time.

### Transparency

When explaining this role to friends and family, they expected I would get access to, _"secret company information"_.
This is not the case.
Since GitLab lives by its transparency value, there's no information I have now that I wouldn't have had in my previous role.
I just have the time to dig into it and look at it.

I'm being as transparent as possible throughout this process.
Not only with posts like this one, but with most aspects of the work.
If you want to see what I'm working on and continue to follow my journey, take a look at the [public GitLab project](https://gitlab.com/gitlab-com/chief-of-staff-cto).
If you want to ask me any questions about the role, check the links in the footer and get in touch.
If you work at GitLab and you're interested in doing this role yourself, [register for it](https://about.gitlab.com/handbook/engineering/cto-staff/#registration-process), or come speak to me.
