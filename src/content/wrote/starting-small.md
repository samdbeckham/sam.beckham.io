---
title: "Starting small"
date: 2022-05-07
description: "The secret to getting something done is simple. Go ahead and do it."
background: "#8BC34A"
tags:
  - productivity
  - software development
  - iteration
  - problem-solving
  - taking action
---

The secret to getting something done is simple. <br/> Go ahead and do it.

## Boring Solutions

At GitLab we embrace [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions). We look for the smallest possible thing we can ship that gets us on the right path.
For an example, take a look at the Pipeline Authoring Group's [Pipeline Editor](https://docs.gitlab.com/ee/ci/pipeline_editor/).
We wanted to build a new editor that would help users create, edit, and update their pipeline configurations.
When we started discussing it, we thought big:

_"Wouldn't it be cool if we could visualize the pipeline you were building in **real time**?"_

_"Imagine writing a pipeline config with **syntax highlighting**, **auto suggestions**, and all the other features our IDE's give us!"_

_"Could we build a pipeline **visually** and have the editor convert everything back into a yaml configuration file?"_

_"What if we had a **library of common components** that we could include right into our configurations from the editor?"_

With all those ideas in our minds, we started thinking small.
What was the boring solution?
What was the smallest possible thing we could ship that set us on the right path to iterate towards all the great features we dreamed up?

Our boring solution?
A text area.

We dialled everything back and introduced the feature as a [readonly text editor](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/46119) that loaded in your `.gitlab-ci.yml` configuration.
After that, we allowed you to edit it, then [commit it those changes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/47083) into your repo.
It wasn't fancy, or groundbreaking.
It had none of the awesome features we originally intended for it.
It was everything it needed to be, and nothing else.
It was boring.

## Iterate

The key part of a boring solution is that it puts you on the path to iterate.
Without this crucial step, our pipeline editor would still be little more than a text area.
It's been a little while since we shipped our boring solution, and we've iterated on it from there.
We've added a lot of the features we originally planned out and many more we hadn't thought about.
It's now a key part of the pipeline experience in GitLab and is a truly awesome feature.

Starting small and gradually adding functionality offered several benefits:
Firstly, there was something useful that we could start gathering feedback on after a matter of weeks.
The more feedback we got, the more we added to it.
The more we added to it, the more people used it.
The more people used it, the more feedback we got.
The more informed we were, the better our feature developed.

Little bits, over time.
With something useful at each step.

## Not just software

This idea extends beyond software.
It's fared me well in many aspects of my life and career.
Another example comes from [starting our meetup](/wrote/starting-a-meetup), [Frontend NE](https://frontendne.co.uk).
When starting Frontend NE, we talked about all the things it could become and everything we wanted to achieve with it.
We talked about conferences, speakers, online courses, and all sorts of things.
But we knew if we were going to start this thing, we should start small.
So we started a Twitter account, rented a free back-room in a co-working space and had [one of the organizers give a talk](https://www.youtube.com/watch?v=-jh0rHHvIzw).
The bare-minimum.

From there, we iterated.
We got a better website, a ticketing system, a bigger room, better AV equipment, sponsors, merch, seasoned speakers, and more.
Each part, gradually, over time.

Five years later and we were in a much bigger space with a regular turnout of 100 folks.
We'd ran two sold-out conferences and had a semi-successful [YouTube channel](https://www.youtube.com/c/FrontendNE/videos?view=0&sort=p&flow=grid) with (almost) all our talks recorded and uploaded.
We would never of had this if we hadn't started small.
We wouldn't have achieved the success we had if we hadn't iterated.

## Throw it away

Another benefit of starting small is that it makes it much easier to throw away things that don't work.
If we held our first meetup and no one showed up, we wouldn't have lost much.
A little bit of time and a small amount of money, but nothing compared to if we started big and then realized there was no appetite for it.
Starting small gives you a safer space to make mistakes and pivot.

Having small iterations allows you to test out ideas.
Maybe you're adding a feature no one needs, or doesn't really solve the problem you thought it would.
With small iterations, that's fine.
You can remove it, or pivot on the idea with relatively little time wasted.

Don't be afraid to throw things away.

## What's stopping you?

So where could a boring solution help you?
What are you procrastinating on that you could just strip back and get started today?

What's stopping you?
