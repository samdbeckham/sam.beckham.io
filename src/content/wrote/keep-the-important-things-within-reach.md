---
title: "Keep the important things within reach"
date: 2021-10-26
description: "Make sure there's as few steps as possible between you and your most important tasks"
background: "#5c1f87"
tags:
  - Productivity
  - Time Management
  - Workflow Optimization
  - Task Management
  - Tools and Applications
---

The best advice I received when learning to play guitar, was to **keep it within reach**.

Don't put it away, in a bag, in a cupboard, or in a room you don't go in.
Keep it to hand so that when you get a spare few minutes you pick it up and play it.
Make sure there are as few steps as possible between whatever you're doing and playing the guitar.
Do this and you'll practice more, learn faster, and be a better player.

This extends to my day-to-day life, especially at work.
I need to make sure there's as few steps as possible between me and my most important tasks.
For me, these tasks are usually:

1. [Adding items to my todo list](#adding-items-to-my-todo-list)
2. [Accessing my 1:1 agendas](#accessing-my-11-agendas)
3. [Searching the GitLab handbook](#searching-the-gitlab-handbook)
4. [Finding bookmarked GitLab issues or MRs](#finding-bookmarks)

For most of these tasks I use [Alfred](https://www.alfredapp.com/).
Alfred is a Mac app that predates the built-in spotlight search and offers many similar features.
You can create your own workflows with simple flowchart based logic, or break out into code and make them as powerful as you need.
The majority of mine fall into the former category as I'm a firm believer in the boring solution.

## Adding items to my todo list

I have a regimented system for managing my todos.
The heart of it requires me to add todos to an inbox for later triaging.
Usually, I'm adding todos mid-meeting or mid-conversation.
The faster I can do this, the better.

I used to use [Alfred Workflow Todoist](https://github.com/moranje/alfred-workflow-todoist) for this.
It's a little buggy though, and the Todoist Mac app has a much nicer shortcut so now I use that directly.
In a couple of keystrokes, I can start typing in whatever I need to do and send it to my todo list inbox.

![Using Todoist's quick-add function](@/assets/images/articles/easy-access-to-the-important-things/todoist.gif)

## Accessing my 1:1 agendas

I have [automated the creation of my meeting agendas](/wrote/automate-your-meeting-agendas) but I still need to access them quickly.
This could be to add items to the agenda, look up past items, or have it open during a meeting.
I tried handling this with bookmarks (which I still do for other things) but I needed even faster access.
This required a custom workflow.
Thankfully, Alfred has a built in List Filter method.
I used this to set up a list of all my meeting agenda urls, making them searchable by title.
All I need to do is type `11 che…` and I get my 1:1 with Cheryl, or `11 auth…` for the Pipeline Authoring group meeting.
Hitting enter opens up the agenda and we're away.

![Searching through my 1:1 documents](@/assets/images/articles/easy-access-to-the-important-things/11-dial-m-for-mireya.png)

## Searching the GitLab handbook

The GitLab handbook is the source of all knowledge for everything GitLab.
Unfortunately, the search functionality isn't amazing (though it is getting better).
Fortunately, it's public so we can use the power of Google site search.

```url
https://www.google.com/search?q=site:https://about.gitlab.com/handbook/ {query}
```

All we need to do is replace `{query}` with whatever we want to search.
This gives us a Google results page scoped specifically to the GitLab handbook.
Fantastic.

Alfred makes this even easier.
I can run `handbook {query}` in Alfred and it opens my browser with the search results.
It's simple and boring, but effective.

## Finding bookmarks

I have a lot of links that are important to access quickly, but change on a semi regular basis.
The planning, retrospective, and release post issues for the current milestone.
OKRs, shared goals, hiring trackers, etc.
They're all links that I'll need to reference several times, but not as permanent as a 1:1 or meeting agenda.
For these, I use the browser's built in bookmarks feature.
To make it one step lazier, I use [Alfred Firefox Bookmarks](https://github.com/nikipore/alfred-firefoxbookmarks).
This way, I type `ff {query}` into alfred and get a list of all the bookmarks that match that query.
As with most of the others, it's simple but saves me a lot of time.

## All the others that I haven't named

These are my most used workflows, but they're not the only ones I use.
I have one that [controls Spotify](https://alfred-spotify-mini-player.com/), one that starts a [pomodoro timer](https://github.com/vitorgalvao/sandwichtimer), one to search our org chart, and many more.
The point here is not about the actual tools I use.
It's about the practice of keeping the important things in reach and making repetitive tasks as quick as possible.
Each task on its own will only save me a few seconds to a minute, but I average 50 uses of alfred a day.
It all adds up.
