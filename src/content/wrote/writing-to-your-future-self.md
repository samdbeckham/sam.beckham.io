---
title: "Writing to Your Future Self"
date: 2021-12-24
description: "Do your future-self a favour and write down today what you want to read tomorrow."
background: "#7CB342"
tags:
  - self-reflection
  - journaling
  - management journey
  - personal growth
  - time management
---

We're coming close to the end of another year and it's time to self-reflect.
How can we accurately remember all we did in the last year?
What if we could reach out to our past-selves and ask them?

Whilst we can't travel back in time, we can leave something for our future-selves to discover.
This is what I've been doing in 2021.
I've been keeping a journal of my management journey.
My accomplishments, my mistakes, how I'm feeling, and where I want to improve.
It's hastily typed, full of spelling errors, and not for public consumption; but it's the most useful document I have.

I started this as a daily thing, but it became cumbersome and I'd go weeks without adding to it.
So I iterated and came up with a process that prompted me to fill it in weekly.
Every Friday I ask myself the following questions:

1. **How do you feel about the past week?**
2. **What progress did I make on my top priority?**
3. **What is the top priority next week?**
4. **What was my biggest success or failure this week?**
5. **How confident do I feel as a manager?**

They're all free-text and I can write as little or as much as I please.

Like most projects, I over-engineered it.
I used bash prompts to answer the questions and auto-commit the file to a repo.
Then I use [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) to build out the repo into a **private** website (see public [example](https://samdbeckham.gitlab.io/week-in-review-template/)).
This process helps me focus and makes it difficult to do any real editing of the posts.
It prevents me from spending too long poring over what I've written and trying to make it perfect.
The entries are supposed to capture my mood in the moment.
The less editing the better.

## For your eyes only

Whilst I advocate transparency in most matters, this one should be confidential.
It's a 1:1 with yourself and you should be comfortable to say what's on your mind.
Keeping it opaque prevents you from overthinking and worrying about other readers.
You can rant, rave, swear, and be as contrarian as you like.
This is your document for you, and you alone.

With that said, I recently shared some select passages with another Engineering Manager.
They are in their first year as an EM and I'm helping them through some of their challenges.
Being able to share certain passages from when I was having a tough time helped to show them they weren't alone.
It helped curb their imposter syndrome a little knowing I'd been through the same thing.
This is a topic for another day though.

## Reflect

As cathartic as furiously typing into the void is, it's much more helpful if you go back and read what you wrote.
After all, past us went to all the effort to **write it down**, the least future us can do is **read it**.

Management is a long-game and it can be tricky to step back and see the whole picture.
Being able to read back through your year (and beyond) gives you the opportunity to spot mistakes you made.
You can watch yourself make poor decisions and learn how to better avoid them.
You can marvel at your own resilience and pay attention to how you pulled yourself out of any pitfalls.
The most cathartic thing by far, is watching yourself struggle with tasks that you now find simple.
It's a great tool for seeing how far you've come on your journey and how you can continue to improve.

## New Year, new you

If you didn't start doing this a year ago, there's not a lot to reflect on.
But why not start now?

To make it easier to start, I've made the starter for the system I use public.
You can have it as a [Google Doc](https://docs.google.com/document/d/1kAf3H8uGNaeP6OmHgtZUzh7jUYKlAIEyZacuMfe0V6U/edit?usp=sharing), or fork the [over-engineered version](https://gitlab.com/samdbeckham/week-in-review-template) I use.
Use whichever will work the easiest for you and allow you to [keep it within reach](/wrote/keep-the-important-things-within-reach/).

Do your future self a favour and start writing to them today.
