---
date: 2018-05-23
title: Going transparent
background: "rgb(252, 163, 39)"
description: "I've started a new role at GitLab as a Frontend Engineer and will be doing all my work out in the open."
tags:
  - Transparency
  - Open Source
  - GitLab
  - Frontend Development
  - Career Transition
---

As of today, I'll be [going transparent](https://www.youtube.com/watch?v=fA9niWjui68&feature=youtu.be&t=22s).

I've started a new role at GitLab as a Frontend Engineer and will be doing all my work out in the open. Every issue, every commit, every comment, all publicly viewable on my [Gitlab profile](https://gitlab.com/samdbeckham).

I use Gitlab a lot. I use it for [Frontend NE](https://frontendne.co.uk), [IHNIWID](https://www.youtube.com/playlist?list=PLQnVLZV0MsRKVTJvKDTFsyWYRLn499JP6), and at [Bytemark](https://bytemark.co.uk). You rarely get the chance to work on something you use every day. I'm very humbled to be given this opportunity. Not only is Gitlab's software great, but their values are too. They come from an open source background so everything is public. From all the code that makes up GitLab, to how they calculate employee salaries.

Being transparent comes with many benefits. As a new starter, I could look through the company handbook, see what my team is working on, and get a feel for the volume and pace of work. Going forward, it means my code will be open to scrutiny from my peers, the public, and Gitlab customers. Whilst a scary thought, it does mean that I'll be more diligent with the code I output. I'll put more thought in to the Merge Requests I make, and be more clear and precise in my issue comments. In turn, I'll become a better developer.

Unfortunately this does mean that I've had to leave Bytemark. They've been very good to me over the last couple of years and I'll miss them all dearly. They're hiring for my old job, so if you're a front-end dev looking for a new challenge, [take a look](https://careers.bytemark.co.uk/), and tell them I said, "Hi".

![The GitLab Tanuki logo](https://cdn.sanity.io/images/zmu7yd1l/production/efe934740cddf1f616df19f26e3b611f11261a19-622x682.png?w=622&q=100&fit=max&auto=format&dpr=2)
