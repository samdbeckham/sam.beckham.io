import fs from "fs";
import { createCanvas } from "canvas";

const dir = "./static/social_images";

const WIDTH = 1280;
const HEIGHT = 670;
const DEFAULT_COLOR = "#31394e";

const generateSocialImage = ({ background, title }) => {
  const filename = title.toLowerCase().replaceAll(" ", "-");
  const capitalizedTitle = title.toUpperCase();
  const padding = 50;
  const canvas = createCanvas(WIDTH, HEIGHT);
  const context = canvas.getContext("2d");

  fillBackground(context, background);

  const textSize = 93;
  const lineHeight = 1.4;

  const style = `bold ${textSize}pt Helvetica`;

  context.font = style;
  context.textAlign = "right";
  context.textBaseline = "center";
  context.fillStyle = "#fff";

  const lines = getLines(context, capitalizedTitle, WIDTH - padding * 2);
  lines.forEach((line, i) => {
    context.fillText(
      line,
      WIDTH - padding,
      HEIGHT - padding - i * textSize * lineHeight
    );
  });

  const buffer = canvas.toBuffer("image/png");
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  fs.writeFileSync(`${dir}/${filename}.png`, buffer);

  return `/${dir}/${filename}.png`;
};

function getLines(ctx, text, maxWidth) {
  const words = text.split(" ").reverse();
  const lines = [];
  let currentLine = words[0];

  for (let i = 1; i < words.length; i++) {
    const word = words[i];
    const newPhrase = `${word} ${currentLine}`;
    const width = ctx.measureText(newPhrase).width;
    if (width < maxWidth) {
      currentLine = newPhrase;
    } else {
      lines.push(currentLine);
      currentLine = word;
    }
  }
  lines.push(currentLine);
  return lines;
}

function fillBackground(context, background = DEFAULT_COLOR) {
  const colorMatchExp = /(#(?:[0-9a-fA-F]{3}){1,2})|(rgba?\([^\)]*\))/;
  const [firstColor] = background.match(colorMatchExp);
  context.fillStyle = firstColor;
  context.fillRect(0, 0, WIDTH, HEIGHT);
}

export default generateSocialImage;
