import cosineSimilarity from "compute-cosine-similarity";
import { getEntry } from "astro:content";
import formatDateForPostCard from "@/helpers/formatDateForPostCard";
import embeddings from "../data/embeddings.json";

function getPostSimilarity(a, b) {
  return cosineSimilarity(a.embedding, b.embedding);
}

function getRecommendations(slug) {
  const post = embeddings.find((e) => e.slug === slug);
  if (!post) {
    return [];
  }
  const ranked = embeddings
    .sort((a, b) =>
      getPostSimilarity(post, a) > getPostSimilarity(post, b) ? -1 : 1
    )
    .map((e) => e.slug);
  return ranked.slice(1, 4);
}

const hydrateRecommendations = async (parentSlug) => {
  const posts = await Promise.all(
    getRecommendations(parentSlug).map(async (slug) => getEntry("wrote", slug))
  );
  return posts.map(({ data: { title, date, background }, id, slug }) => ({
    id,
    title,
    background,
    subtitle: formatDateForPostCard(date),
    path: `/wrote/${slug}`,
  }));
};

export default hydrateRecommendations;
