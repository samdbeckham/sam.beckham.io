import { format } from "date-fns";

const formatDateForPostCard = (date) => format(new Date(date), "MMMM do, yyyy");

export default formatDateForPostCard;
