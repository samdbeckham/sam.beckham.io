function prefetch(path) {
  const link = document.createElement("link");
  link.rel = "prefetch";
  link.href = path;

  document.head.appendChild(link);
}

export default prefetch;
