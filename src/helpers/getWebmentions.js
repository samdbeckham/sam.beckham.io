import allWebmentions from "../data/webmentions.json";

const getWebmentions = (slug) =>
  allWebmentions.filter((mention) => mention.target.includes(slug));

export default getWebmentions;
