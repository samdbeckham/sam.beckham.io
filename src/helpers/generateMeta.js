const generateMeta = (key, content) => ({
  name: key,
  property: key,
  key,
  content,
});

export default generateMeta;
