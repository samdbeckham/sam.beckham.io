const getMastodonShareUrl = ({ url }) =>
  `http://www.mastodonshare.com/?url=${url}`;

export default getMastodonShareUrl;
