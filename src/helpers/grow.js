import isReduced from "./isReduced";

const FULLSCREEN_CLASS = "fullscreen";
const TRANSITIONING_CLASS = "transitioning";

function reset(el) {
  el.style.transform = "";
  el.classList.remove(TRANSITIONING_CLASS);
}

function grow(el) {
  return new Promise((resolve) => {
    if (isReduced()) {
      resolve();
    }

    const start = el.getBoundingClientRect();
    el.classList.add(FULLSCREEN_CLASS);

    const end = el.getBoundingClientRect();
    el.classList.remove(FULLSCREEN_CLASS);

    const translate = `${end.left - start.left}px, ${end.top - start.top}px`;
    const scale = `${end.width / start.width}, ${end.height / start.height}`;

    el.classList.add(TRANSITIONING_CLASS);
    el.style.transform = `translate(${translate}) scale(${scale})`;

    el.addEventListener("transitionend", () => {
      reset(el);
      resolve();
    });
  });
}

export default grow;
