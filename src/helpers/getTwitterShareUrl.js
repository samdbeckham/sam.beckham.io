const getTwitterShareUrl = ({ url }) =>
  `http://www.twitter.com/share?url=${url}`;

export default getTwitterShareUrl;
