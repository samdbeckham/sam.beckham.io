const getObserver = (options) =>
  new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      entry.target.$_intersectionHandler(entry);
    });
  }, options || {});

export function createObserver(
  el,
  appearCallback = () => ({}),
  disappearCallback = () => ({})
) {
  const observer = getObserver();

  el.$_intersectionHandler = (entry) => {
    if (entry.isIntersecting) {
      appearCallback();
    } else {
      disappearCallback();
    }
  };
  el.$_intersectionObserver = observer;

  observer.observe(el);
}

export function destroyObserver(el) {
  el.$_intersectionObserver.unobserve(el);
  delete el.$_intersectionHandler;
  delete el.$_intersectionObserver;
}

export default createObserver;
