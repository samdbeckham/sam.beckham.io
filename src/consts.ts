// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = "Sam Beckham";
export const SITE_DESCRIPTION =
  "The personal website, blog, portfolio, and playground of Sam Beckham. Frontend engineering manager at GitLab";
