import rss from "@astrojs/rss";
import { getCollection } from "astro:content";
import MarkdownIt from "markdown-it";
import { SITE_TITLE, SITE_DESCRIPTION } from "../consts";

const parser = new MarkdownIt();

export async function GET(context) {
  const posts = (await getCollection("wrote")).sort(
    (a, b) => new Date(b.data.date) - new Date(a.data.date)
  );
  return rss({
    title: SITE_TITLE,
    description: SITE_DESCRIPTION,
    site: context.site,
    items: posts.map((post) => ({
      ...post.data,
      link: `/wrote/${post.slug}/`,
      pubDate: post.data.date,
      content: parser.render(post.body),
    })),
  });
}
